## If you want to contibute contact Spaceslug <me@spaceslug.no> and I will write an actual CONTRIBUTING guide.

## How to generate a release

- The commit to main thats is the release version must have a tag which is the version number. Rules for version number here [link TODO]
- The tag name can only consist of 'v' and then the version number.  Ex. 'v.3.2.13'
- The name of the release should be 'Slugchess v3.2.13 ' then some text.
- The release must have included the packages built versions of the release in the form 'https://slugchess.com/download/releases/linux-x64/slugchess_0_20_0_linux-x64.7z'  ['linux-x64','win-x64'] and slugchess_[version]_[target].
- The strict release prosedure is because the automated updater uses the gitlab releases for updating itself.
- The slugchess updater will look at the gitlab projects releases to find which versions are available
