#!/bin/bash
install_folder=$HOME/.local/lib/SlugChess/
# do not change these
bin_folder=$HOME/.local/bin
if ! command -v 7z &> /dev/null
then
    echo 7zip not installed. You must have 7zip to run this installer.
    exit
fi
echo This will install SlugChess to $HOME/.local/lib/SlugChess and delete the old files
while read -p "Continue [Y/n]: " x; do
    x=${x:-y}
    if [ $x = 'y' -o $x = 'Y' ]
    then
        break
    fi 
    if [ $x = 'n' -o $x = 'N' ]
    then
        exit
    fi
done
rm -rf "$install_folder"
mkdir -p "$install_folder"
mkdir -p "$HOME/.local/share/SlugChess"
mkdir -p "$HOME/.config/SlugChess"
tmp_dir=$(mktemp -d -t slugchess-XXXXX)
wget -P $tmp_dir https://slugchess.com/download/latest/linux-x64/slugchess_latest.7z
7z x $tmp_dir/slugchess_latest.7z -o$tmp_dir/ext
mv $tmp_dir/ext/SlugChess/* "$install_folder"
rm -rf $tmp_dir
# set execution bit
chmod -v u+x "${install_folder}SlugChess"
chmod -v u+x "${install_folder}SlugChessUpdater"
#make symbolic link in $HOME/.local/bin to SlugChess and SlugChessUpdater
ln -sf "$install_folder/SlugChess" "$bin_folder/slugchess"
ln -sf "$install_folder/SlugChessUpdater" "$bin_folder/slugchess-updater"
if [ $(echo $PATH | grep -c $HOME/.local/bin) -eq 0 ]
then
    echo "slugchess and slugchess-updater added to $HOME/.local/bin but $HOME/.local/bin is not in you local \$PATH. You must add $HOME/.local/bin to you \$PATH use those commands" 
fi
rm -rf $tmp_dir
rm install-slugchess.sh
echo Installation complete
