## Not assigned to version yet
- ~~propper user login system (Make Loggin call a lifetime call)~~
- ~~sign the assembly with a certificate to make windows happy~~
  - seams i have to pay money to get this
- ~~learn how to debug in the IDE. It sucks for c++~~
- ~~go back and forwards in the move while playing a match~~
- ~~print the Rules of a SlugChess game to text~~
- print the VisionRules of a SlugChess game to text
- add commands to the server in the message box
- ~~updator of slugchessclient (fetch newest version for spaceslug.no)~~
- client with only unary and server streamer calls for Match and ChatMessages
- ~~server saves all matches that have been played~~
- password and name on hosted games
- Implement rate limiting of grpc calls per user
- make .AppImage for Aval https://github.com/AppImage/AppImageKit/wiki/Bundling-.NET-Core-apps
- Add unit testing framework and add some tests.

## What does the versions mean (only from v1.0.0 and onwards)
The client and server versions follow each other. It is impossible to use a release client with 
higher number than release server as new client is only release after server is updated.

### Build version is lower
It is recomended to update. Hosted games can still be played.

Minor fixes goes here that should not break games between users

### Minor version is lower
You need to update in order to login

Game breaking changes but no changes to the chesscom communication api or function

### Major
Changes to chesscom commuication api or underlating function.

## BUGS:
- might be race condition when surrendering
- Emoji not working in textbox. Will probably be fixed in avalonia 0.11
- DataGrid Sometimes not converting Timestamp correctly when getting replays from server. Avalonia issue

### version 3.0.0
- [ ] use json save app state on Aval
- [ ] server also save state so user can log back in on crash and resume game
- [ ] use propper data storage for user and other to guard from data corruption due to power failure
- [ ] GetListOfGamesDatabase need caching of game names. Proper data storage will fix this.
- [ ] Implement the timestamp before and after filter for QueryGamesDatabase call in SCS.

### version 2.0.0
- [ ] add matchmaking back with elo. Make sure the the PGN is diffent on matchmaking games then in custom games.
- [ ] implement custom games rules again.
- [ ] rpc AvailableGames(UserIdentification) returns (HostedGamesMap) {} // can be infinetly long and need a pageing or streaming system.
- [ ] in a chess match you can see your opponents data and the rules for this game.
- [ ] make a system for printing a game rules to text and display it when selecting games in the lobby menu.
- [ ] Add Event to to PgnInfo sent over Chesscom. Can also be a filter for pgn search on server
- [ ] Change chat system to use rooms instead of user to user.

### version 1.1.0


### version 1.0.2
- [ ] BUG - Server replays is broken. Some datetime thing is wrong
- [ ] BUG - Filepicker folder on flatpak is stupid
- [ ] BUG - Clicking 'View Replays' crashes the game before you have played your first game
- [ ] Add settings menu for SCC.
  - [ ] (setting) In Torch Mode hovering should show viewdistance of the piece.
  - [ ] Use json to save setting in client, like remebering username and password.
- [ ] Make system for anon users. Ex: have a default anon password and user 'anonimous' you can one click log in as.

### version 1.0.1
- [x] create propper webpage for slugchess.com and host the files there or gitlab.
- [x] Flatpak support arm
- [x] Make it possible to run SCC and SCS without ssl.


### version 1.0.0
- [x] server certs is fucked.
- [x] make Flatpak for Aval (missing some server connection)
- [x] add licences nessesary, (headers to much work) add lisence fir SCS SCC and SC. Cli for SCS, SCC and about button for SC.
- [x] Remove "Type /help" text on login
- [x] Create new, better icon (Endre is on it)
- [x] Add text chatbox, "Only works when in active game"
- [x] bug - selecting a piece with no available move does not put a green ring around it.
- [x] bug - while(xxxxx) in PingService 'The CancellationTokenSource has been disposed'. Happens when game ends
- [x] TEST WINDOWS VERSION (can not test in bottle because Bottles does not have .NET 7 runtime yet)

### version 0.23.0
- [x] upgrade to Avalonia 11
- [x] make this a stable version
- [x] add TLS to transport if grpc. Client and server gets certificate built in binary
- [x] last oppertunity to change SightWip and TorchWip rules. Remove Wip from name.
- [x] somehow make emoji work with Avalonia, in May 2021 not properly supportet yet. see https://github.com/AvaloniaUI/Avalonia/pull/4893
- [x] Ask for confimation when pressing Surrender.
- [x] When selecing textbox in PlayView arrow presses are handled by chessboard.

### version 0.22.1
- [x] "Go back" should be on left side
- [x] Is global exit a good button when an exit button is on the titlebar. Maybe on exit button on main menu.
- [x] "Go Back" button should be disabled when playing in an ongoing match. "Surrender" shoud be the only surrender button
- [x] Lobby remove leave button. Remove Start and Kick buttons for Observer and Challenger.
- [x] UnSelects unit when enemy does move
- [x] bug - When connecting the message should shift to "Connected To Server" so the user knows it is connected.
- [x] bug - Is both login and register button of same gigasize?
- [x] bug - Login screen gives to different messages when failed.
- [x] bug - Selected row in find game is very wrong. Need better colors. Maybe muted grays. Note: hard to change away from blue.
  - [x] Join Game and such button shoud be on bottom.
- [x] bug - searching replays from server results in grpc error
- [x] bug - When server sends ShadowMoves to the client. Is sends the ShadowMoves the opponent should have gotten. And the 
opponent gets what you should have gotten.
- [x] made shadow move highlight orange

### version 0.22.0
- [x] castling checks threatened fields, and works with FisherRandom
- [x] SCC can not process O-O and O-O-O in san.
- [x] chessboard coloring in fog needs a fixing up.
- [x] search TODOs and fix them or add as a new task
- [x] rework lobby screen
- [x] make a first propper release
- [x] server can detect when a player ran out of time, and stop the client form detecting it
- [x] bug - Sending chat messages not working

### version 0.21.0
- [x] split the github repos and use git submodules for handeling dependencies. Move to gitlab.com
- [x] auto updator can update to spesific version. A command line option. Mayby post link to available releases page
- [x] swap grpc api to worker5271.slugchess.com
- [x] add no server available message on main screen
- [x] bug - fix sound on linux: just needed alsa config for aplay
- [x] bug - can not view replays when not connected: server is required because it parses the file. Client can not do it

### version 0.20.1
- [x] go over what lines are logged on the server
- [x] go over chesscom grpc calls and make sure to require authentication

### version 0.20.0
- [x] implement a ping service 
- [x] a check to evaluate the validity of time spent on move. Change the value if it seems wrong. 
- [x] in lobby give admin time to accept player.
- [x] option to not allow observers in lobby
- [x] option to kick observer
- [x] updated the dockerfile build and deploy system 
- [x] if a player leaves before whites first move or blacks first move, just cancel the match and give no loss. (Forces draw, not yet tested)
- [x] bug - signal handling fixed on SCS
- [x] bug - host can start game if clicking fast after joiner clicks Exit
- [x] bug - fog not appering

### version 0.19.1
- [x] arrows to move forward and backwards
- [x] updated updater to net6.0. no longer backward compatible
- [x] bug - joiner of game sometimes not get past lobby. Mayby not get MatchStart before listener is attached.
- [x] bug - server often fails to get userdate when debugging `Logger::log(Logger::LogLevel::Error,"Failed to load userdata");`
- [x] bug - clock view is broken
- [x] bug - killed pieces view is broken, it does not show up until many pieces is killed

### version 0.19.0
- [x] make a propper logger on server. Write to log file and stdout. Delete logfile on restart
- [x] spectate a PGN (PGNs on server)
- [x] spectate a live game
- [x] net6.0
- [x] enable autoupdate again on linux


### version 0.18.0 (Done)
- [x] make a propper logging system on Aval
- [x] GRPC C++ updated on winter to 1.39.0
- [x] ~~deprecate~~ fixed autoupdate on linux. when in /usr/local/bin you need sudo to update the files. Where to put user data https://www.freedesktop.org/software/systemd/man/file-hierarchy.html. Put whole program in ~/.local/lib/SlugChess/ , put a symlink to exec in ~/.local/bin/slugchess and ~/.local/bin/slugchess-updater, shared resources like replays in ~/.local/share/SlugChess/ , configs and log files in ~/.config/SlugChess/
- [x] rename bin SlugChessAval to SlugChess

### version 0.17.0 (Done)
- [x] make a propper version system for slugchesscore shared lib. A build of slugchess server needs a spesific version of core. When you publish server release you supply a core version (think more on it). symlink to more spsific version to support multiple versions. Currently slugchesscoretest rpath does not work and will only use export version of lib
- [x] makefile SlugChessServer have a define for debug build. Compile different .o files for debug and regular
- [x] rename 'libslugchess.so' to 'libslugchesscore.so'
- [x] clean illegal filesystem names from usernames in replays
- [x] updated SlugChessAval to dotnet5.0

### version 0.16.1 (Done)
- [x] Aval: Went through a lot of the text on SlugChessAval and improved it
- [x] Proto: added Logout and NamedVariants 
- [x] Server: can now logout 

### version 0.16.0 (Done)
- [x] Formalize Torch and Sight. Both a text and ~chesscom~ description of the rules. Formalized in SlugChessCore They should temporerly be known as TorchWip and SightWip. 
- [x] password system
- [x] save userdata {username as id and elo}
- [x] simple elo system (same as FIDE)

### version 0.15.1 (Done)
- [x] use NetCoreBeauty to clean up root dir
- [x] Updator copy updator to x.new and Aval will rename x.new to x. Thus ensuring updator also gets updated


### version 0.15.0 (Done)
- [x] load up and click through PGNs.
  - [x] Make Match.Model propper observable type. Less of the properties. More observable.
  - [x] Make server set bool if PGN was parsed successfully.
  - [x] select viewpoint when watching replay
  - [x] add no_vision_rules as a vision rule to Core
- [x] Bind ServerVisionRulset with CreateGame view selector. 

### version 1.14.1
- [x] bug- Server kan motta move etter match er ferdig. Fører til krash
- [x] (could not replicate)bug- Server fjerner ikke bruker ordentlig ved heartbeat failed
- [x] Updater new retries when .7z handle not released during cleanup

### version 0.14.0
- [x] record games played (PGN). In a replay folder for users and a replay folder on server

### version 0.13.1 (Done)
- [x] client can be lagging behind 1 message when finishing matches
- [x] nothing happens on client when time runs out. When out of time client sends random move
- [x] selecteble pieces with zero moves are sent from server
- [x] sort capture pieces according to color instead of cronologicaly
- [x] auto updator is busted again. looks for files in wring locations

### version 0.13.0 (Done)
- [x] move match logic to MatchModel. Ex: IsCurrentPlayersTurn obs lives there, code for match events is there and the Chesscom call
- [x] if server is wrong version or is not there at all. Show a propper message to the user trying to log on


### version 0.12.1 (Done)
- [x] finish install.sh for linux. make seperate for debug also
- [x] click again on a piece to deselect
- [x] clean up selection colors


### version 0.12.0 (Done)
- [x] add audio to Aval 
- [x] python script for uploading new versions of Aval and maintain a manifest of versions for updater
- [x] auto updator for Aval


### version 0.11.1 (Done)
- [x] clean up TODOs on servinator
- [x] Alive heartbeat must be fixed. Make sure it works as intended.

### version 0.11.0 (Done)
- [x] make messager globaly available in SCS so messages can be sent to a user anywhere in the code
- [x] a worker that periodicly run tasks on server
  - [x] Task: Check last hearbeat on logged in
  - [x] Task: Remove matches that should no longer run
  - [x] Task: Log metrics like ammout of logged in users, current matches
- [x] Do the things that should happen when user is logged out e.g stop matches, end all log calls
- [x] need to separate messages to player. Both can't listen to match_events because they sometime need differert events


### version 0.10.1 (Done)
- [x] leave game and conced
- [x] ask for and accept draw
- [x] see if Draw commands can be made with async commands istead of callin UIAsync https://reactiveui.net/docs/guidelines/framework/asynchronous-commands
- [x] fix cancel on hosting not can   celing hosting

### version 0.10.0 (Done)
- [x] back and forward in match( clients are sendt state so should be easy)
- [x] a way to see where and when pieces died
- [x] switch to aval with all the same features as client
  - [x] Fix match terminating when one player exits client
  - [x] Messages not sent between players in a match
  
### version 0.9.0 (Done)
- [x] implement double fisher random
- [x] fix release uploader

### version 0.8.1 (Done)
- [x] fix looking for game bug
- [x] fix check helper bug, (check mus be handled inside slugchess. shadowmoves is not good enough)
- [x] Make less cases of invalid state on client. Only enable things when logged in
- [x] implement Alive Hearbeat (Clients must send heartbeat every 1 min)

### version 0.8.0 (Done)
- [x] Create custom games with custom rules
- [x] create a new object orientet structure to the server code

### version 0.7.1 (Done)
- [x] create a new object orientet structure to the server code
  - [x] rework some thread to use conditional varible instead of thread sleep. This to improve speed
- [x] Print SAN at end of game

version 0.7.0 (Done)
-(x)chose new IDE to work with on linux
-(x)move all move validation to server/rules not on server
  -(x)slugchess on server (missing end of game)
-(x)critical game breaking bug. Server stops excepting massages and computer crashes
-(x)timer is fucked no time is removed

version 0.6.2 (Done)
-(x)sound when running out of time
-(x) bug - time over minutes of time used are ignored

version 0.6.1 (Done)
-(x)finaly implement view move field (have vision on all tiles where your pieces can move to)
    -calling this Sea rules

version 0.5.2 (Done)
-can click on opponents pieces to see moves

version 0.5.1 (Done)
-(X)prevent move through hidden fields

version 0.4.2 (Done)
-(x)remove unessesary files like x86 stuff and .xml and .pdb

version 0.4.1 (Done)
-(x)chess clock

version 0.3.3 (Done)
-(x)bug: last move not cleared when starting a new match.
-(x)bug: text log only scrolles when "Opponet did move!" or opponent won

version 0.3.2 (Done)
-(x)make it a only x64 program 

version 0.3.1 (Done)
-(x)(optional rule) Last captured field is visible for both players
-(x)chat system
-(x)bug: losing player can continue to play
-(x)bug: loser does not get message that they lost. Server does not send message

version 0.2.4 (Done)
-(x)add debug port
-(x)checker of version number. Server must match clients. Minor not same is fine.
-(x)fix no message scroll after match is done
-(x)fix killed pieces not cleared after new match is started
-(x)add sound when match found

version 0.2.3 (Done)
-(x)fix that user that does not have current turn freezes on exit
--(x)changing to a dedicated read thread on server
-(x) added autogeneration of csharp proto 

version 0.2.2 (Done)
-(x) make autoexport to spaceslug.no
-(x) make service for slugChessServinator
-(x) show check from visible enemies


version 0.2.1 (Done)
-message moveResult contains MatchEvents
-Match rules on server
-add end of game

version 0.1.2 (Done)
-opponent move sound
-probably sleep loop fail
