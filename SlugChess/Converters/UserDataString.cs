using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SlugChess.Converters
{
    public class UserDataString : Avalonia.Data.Converters.IValueConverter
    {
        public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if(value==null) return "";
            var ud = (ChessCom.UserData)value;
            return $"{ud.Username}({ud.Elo:0})";
        }

        public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
