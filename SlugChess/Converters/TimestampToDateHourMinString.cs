﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;

namespace SlugChess.Converters
{
    public class TimestampToDateHourMinString : Avalonia.Data.Converters.IValueConverter
    {

        public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if(value == null) { return "convert value null"; } 
            Google.Protobuf.WellKnownTypes.Timestamp dateTime = (Google.Protobuf.WellKnownTypes.Timestamp)value;
            var s = dateTime.ToDateTime().ToString(@"yy-MM-dd hh:mm");
            return s;
        }

        public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
