#!/usr/bin/env python3
import os, re, uploader

rootDir = os.path.dirname(__file__)
projectDir = os.path.join(rootDir, '..')
#avalBinDir = os.path.join(projectDir, 'bin')
updaterDir = os.path.join(projectDir, '../SlugChessUpdater')
net_ver = "net7.0"
# type can be "Release" or "Debug" 
# targets can be ["win-x64", "linux-x64", "osx-x64"]
def publish(type, targets, net_ver): 
	if type == 'Debug':
		debug = True
	elif type == 'Release':
		debug = False 
	else:
		print('Error: invalid type')

	for target in targets:
		print('Building {} {}'.format(type, target))
		stream = os.popen('dotnet publish {}/SlugChess.csproj -c {} -r {} --self-contained false -p:PublishSingleFile=false -p:PublishTrimmed=false'.format(projectDir, type, target))
		print(stream.read())
		print('Building {} updater {} '.format(type, target))
		stream = os.popen('dotnet publish {}/SlugChessUpdater.csproj -c {} -r {} --self-contained false -p:PublishSingleFile=true -p:PublishTrimmed=false  -o {}/bin/{}/{}/{}/publish/'.format(updaterDir, type, target, projectDir, type, net_ver, target))
		print(stream.read())


	stream = os.popen('dotnet-version --show --project-file {}/SlugChess.csproj'.format(projectDir))
	output = stream.read()
	version = re.search('[0-9].+.+$',output).group(0)
	print('Finished publishing SlugChess ' + version + ' ' + target)
	uploader.prepereAndUpload(version, targets, debug, net_ver)
	print('Finished uploading SlugChess ' + version + ' ' + target)

