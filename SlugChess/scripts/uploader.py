#!/usr/bin/env python3
import os, shutil, py7zr, pysftp, paramiko
from paramiko import Ed25519Key
#from base64 import b64decode
from pathlib import Path
# register file format at first.
shutil.register_archive_format('7zip', py7zr.pack_7zarchive, description='7zip archive')
shutil.register_unpack_format('7zip', ['.7z'], py7zr.unpack_7zarchive)

rootDir = os.path.dirname(__file__)
projectDir = os.path.join(rootDir, '..')
releaseDir = os.path.join(projectDir, 'bin/Release')
debugDir = os.path.join(projectDir, 'bin/Debug')
tempDir = os.path.join(projectDir, 'temp')
net_ver ="net7.0"

def prepereAndUpload(version, targets, debug=False, net_ver="net7.0"):
    print('Preping archives')
    os.makedirs('{}/'.format(tempDir), 0o777, True)
    version = version.replace('.', '_')
    #print(releaseDir)
    for target in targets:
        archive(version, target, debug, net_ver)

#    uploadArchive(targets, version, debug)
    uploadArchiveGlados(targets, version, debug)
    createReleaseGitlab(targets ,version, debug)
    shutil.rmtree(tempDir)


def archive(version, target, debug, net_ver):
    if debug:
        targetDir = os.path.join(debugDir+"/"+net_ver, target)
    else:
        targetDir = os.path.join(releaseDir+"/"+net_ver, target)
    
    os.makedirs('{}/{}/'.format(tempDir,target), 0o777, True)
    print('Archiving ' + target)
    os.rename(targetDir + '/publish', tempDir + '/SlugChess')
    #//shutil.make_archive(rootDir + '..\\temp\\win-64\\SlugChess_'+version, '7zip', releaseDir+'\\win-64\\', 'publish')
    #shutil.make_archive(tempDir + '/'+ target + '/SlugChess_'+version, '7zip', 'publish' , releaseDir + '/'+ target +'/publish')
    make_archive(tempDir + '/SlugChess', '{0}/{1}/slugchess_{2}_{1}'.format(tempDir,target,version))
    os.rename(tempDir + '/SlugChess', targetDir + '/publish')


def make_archive(source, destination):
    base = os.path.basename(destination)

    archive_from = os.path.dirname(source)
    archive_to = os.path.basename(source.strip(os.sep))
    print(source, destination, archive_from, archive_to)
    shutil.make_archive(base, '7zip', archive_from, archive_to)
    shutil.move('%s.%s'%(base,'7z'), destination + '.7z')


def uploadArchive(targets, version, debug=False):
    hostname = open(str(Path.home()) + '/.secrets/spaceslug.no_SFTP_hostname.txt', 'r').read()
    username = open(str(Path.home()) + '/.secrets/spaceslug.no_SFTP_username.txt', 'r').read()
    password = open(str(Path.home()) + '/.secrets/spaceslug.no_SFTP_passord.txt', 'r').read()
    #since pysftp is shit on windows and can not find known_hosts from putty i must disable the security feature
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None   
    
    with pysftp.Connection(host=hostname, username=username, password=password, cnopts=cnopts) as sftp:
        print("Connection with spaceslug.no succesfully stablished ... ")
        sftpUpload(sftp, targets, version, "slugchess", debug)
        
def uploadArchiveGlados(targets, version, debug=False):
    hostname = "glados.lan"
    username = "slugchess"
    private_key = str(Path.home()) + '/.ssh/id_rsa_slugchess'
    #since pysftp is shit on windows and can not find known_hosts from putty i must disable the security feature
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None   
    
    with pysftp.Connection(host=hostname, username=username, private_key=private_key, port=2022, cnopts=cnopts) as sftp:
        print("Connection with glados succesfully stablished ... ")
        sftp.cwd("slugchess.com")
        sftpUpload(sftp, targets, version, "download", debug)

def sftpUpload(sftp, targets, version, prefix="download", debug=False):
    
    #directory_structure = sftp.listdir_attr()

    latest_name = 'slugchess_latest.7z'
    # Print data
    #for attr in directory_structure:
    #     print(attr.filename, attr)
    if debug:
        bType = prefix+'-debug' 
    else:
        bType = prefix+''

    for target in targets:
        filename = 'slugchess_{}_{}.7z'.format(version, target)
        sftp.makedirs('./{}/releases/{}'.format(bType, target), 774)
        sftp.makedirs('./{}/latest/{}'.format(bType, target), 774)
        #sftp.remove('./{}/latest/version.txt'.format(bType))
        f = open('{}/version.txt'.format(tempDir),"w+")
        f.write(version.replace('_', '.'))
        f.close()
        sftp.cwd(bType)
        sftp.cwd('latest')
        sftp.put('{}/version.txt'.format(tempDir), 'version.txt')
        sftp.cwd(target)
        if sftp.exists(latest_name):
            sftp.remove(latest_name)
            print('removed old ' + latest_name)
        sftp.put('{}/{}/{}'.format(tempDir,target,filename), latest_name)
        
        sftp.cwd('../..')
        sftp.cwd('releases')
        sftp.cwd(target)
        if sftp.exists(filename):
            sftp.remove(filename)
        sftp.put('{}/{}/{}'.format(tempDir,target,filename), filename)
        print('Uploaded '+target+' slugchess_'+version)
        sftp.cwd('../../..')
    print("sftp upload complete")     

# connection closed automatically at the end of the with-block

def createReleaseGitlab(targets, version, debug=False):
    dotversion = version.replace('_', '.')

    if debug:
        print("No gitlab release for debug")
    else:
        token = os.popen('kwallet-query -r gitlab-api-token kdewallet -f private'.format()).read()
        token = token.strip(' \t\n\r')

        test_tag = os.popen("curl --http1.1 --header 'PRIVATE-TOKEN: {0}' 'https://gitlab.com/api/v4/projects/38448404/repository/tags/v{1}'".format(token, dotversion)).read()
        print(test_tag)
        if not '"name":"v{0}"'.format(dotversion) in test_tag:
            print("Tag v{0} not found. Exiting".format(dotversion))
            return

        links = '['
        for target in targets:
            filename = 'slugchess_{}_{}.7z'.format(version, target)
            url = 'https://slugchess.com/download/releases/{}/{}'.format(target, filename)
            links += '{{ \"name\": \"{}\", \"url\": \"{}\", \"link_type\":\"package\" }},'.format(filename, url)

        links = links[:-1] + ']'
        
        print(os.popen("curl --http1.1 --header 'Content-Type: application/json' --header 'PRIVATE-TOKEN: {0}' --data '{{ \"name\": \"Slugchess v{1}\", \"tag_name\": \"v{1}\", \"description\": \"## Write this manualy\", \"milestones\": [], \"assets\": {{ \"links\": {2} }} }}' --request POST 'https://gitlab.com/api/v4/projects/38448404/releases'".format(token, dotversion, links)).read())
        #print("curl --verbose --http1.1 --header 'Content-Type: application/json' --header 'PRIVATE-TOKEN: {0}' --data '{{ \"name\": \"Slugchess v{1}\", \"tag_name\": \"v{1}\", \"description\": \"## Write this manualy\", \"milestones\": [], \"assets\": {{ \"links\": {2} }} }}' --request POST 'https://gitlab.com/api/v4/projects/38448404/releases'".format(token, dotversion, links))
