#!/bin/bash
install_folder=$HOME/.local/lib/SlugChess-debug/
# do not change these
bin_folder=$HOME/.local/bin
if ! command -v 7z &> /dev/null
then
    echo 7zip not installed. You must have 7zip to run this installer.
    exit
fi
echo This will install SlugChess to $HOME/.local/lib/SlugChess-debug and delete the old files
while read -p "Continue [Y/n]: " x; do
    x=${x:-y}
    if [ $x = 'y' -o $x = 'Y' ]
    then
        break
    fi 
    if [ $x = 'n' -o $x = 'N' ]
    then
        exit
    fi
done
rm -rf "$install_folder"
mkdir "$install_folder"
mkdir "$HOME/.local/share/SlugChess-debug"
mkdir "$HOME/.config/SlugChess-debug"
tmp_dir=$(mktemp -d -t slugchess-XXXXX)
wget -P $tmp_dir http://spaceslug.no/slugchess-debug/latest/linux-x64/SlugChess_latest.7z
7z x $tmp_dir/SlugChess_latest.7z -o$tmp_dir/ext 
mv $tmp_dir/ext/SlugChess/* "$install_folder"
rm -rf $tmp_dir
# set execution bit
chmod -v u+x "${install_folder}SlugChess"
chmod -v u+x "${install_folder}SlugChessUpdater"
#make symbolic link in $HOME/.local/bin to SlugChess and SlugChessUpdater
ln -sf "$install_folder/SlugChess" "$bin_folder/slugchess-debug"
ln -sf "$install_folder/SlugChessUpdater" "$bin_folder/slugchess-updater-debug"
if [ $(echo $PATH | grep -c $HOME/.local/bin) -eq 0 ] 
then
    echo "slugchess-debug and slugchess-updater-debug added to $HOME/.local/bin but $HOME/.local/bin is not in you local \$PATH. You must add $HOME/.local/bin to you \$PATH use those commands" 
fi
rm -rf $tmp_dir
rm install-slugchess-debug.sh


