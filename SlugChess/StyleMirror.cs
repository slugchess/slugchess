﻿using Avalonia.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlugChess
{
    public static class StyleMirror
    {
        public static SolidColorBrush? BlackField { get; set; }
        public static SolidColorBrush? WhiteField { get; set; }
        public static SolidColorBrush? ThemeControlMidBrush { get; set; }
    }
}
