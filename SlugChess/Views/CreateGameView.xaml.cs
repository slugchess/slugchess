﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using SlugChess.ViewModels;

namespace SlugChess.Views
{
    public class CreateGameView : ReactiveUserControl<CreateGameViewModel>
    {
        public CreateGameView()
        {
            AvaloniaXamlLoader.Load(this);
            this.WhenActivated(disposables =>
            {

            });
        }
    }
}
