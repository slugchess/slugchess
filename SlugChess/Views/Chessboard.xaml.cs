﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avalonia.ReactiveUI;
using Avalonia.Input;
using ReactiveUI;
using SlugChess.ViewModels;

namespace SlugChess.Views
{
    public class Chessboard : ReactiveUserControl<ChessboardViewModel>
    {
        //public Button MyButton;
        public Chessboard()
        {
            this.WhenActivated(disposables =>
            {
                // Bind the 'ExampleCommand' to 'ExampleButton' defined above.
                //this.BindCommand(ViewModel, x => x.ClickShit, x => x.ExampleGrid)
                //    .DisposeWith(disposables);
                StyleMirror.WhiteField = (SolidColorBrush?)this.Resources["WhiteField"] ?? throw new System.Exception("Set this man");
                StyleMirror.BlackField = (SolidColorBrush?)this.Resources["BlackField"] ?? throw new System.Exception("Set this man");
                StyleMirror.ThemeControlMidBrush = (SolidColorBrush?)this.Resources["MyThemeControlMidBrush"] ?? throw new System.Exception("Set this man");


            });
            AvaloniaXamlLoader.Load(this);
            //ViewModel.mainChessboardGrid = this.FindControl<Grid>("mainChessboardGrid");
            //MyButton = this.FindControl<Button>("myButton");
        }

        private void ChessfieldClicked(object o, PointerReleasedEventArgs a)
        {
            ViewModel?.ChessfieldClicked((INamed)o);
        }

        private void ChessfieldEnter(object o, PointerEventArgs a)
        {
            ViewModel?.ChessfieldEnter((INamed)o);
        }

        private void ChessfieldLeave(object o, PointerEventArgs a)
        {
            ViewModel?.ChessfieldLeave((INamed)o);
        }



    }
}
