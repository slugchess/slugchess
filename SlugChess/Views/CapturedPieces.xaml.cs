﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Presenters;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using SlugChess.ViewModels;
using System;

namespace SlugChess.Views
{
    public class CapturedPieces : ReactiveUserControl<CapturedPiecesViewModel>
    {
        private ScrollViewer? _scroller;
        public CapturedPieces()
        {
            AvaloniaXamlLoader.Load(this);
            this.WhenActivated(disposables =>
            {
                CapturedPiecesViewModel? context = (CapturedPiecesViewModel?)DataContext;
                if(context != null)context.PanelWidth = Width;
                //if(((ScrollContentPresenter)_scroller.Presenter) != null)((ScrollContentPresenter)_scroller.Presenter).CanHorizontallyScroll = false;
            });
            _scroller = this.FindControl<ScrollViewer>("scroller");
            //DataContext = new CapturedPiecesViewModel();
        }

    }
}
