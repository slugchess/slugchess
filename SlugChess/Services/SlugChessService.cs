﻿using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.IO;
using Avalonia.Collections;
using Avalonia.Threading;
using ChessCom;
using Google.Protobuf.Collections;
using Grpc.Core;
using ReactiveUI;
using SlugChess.ViewModels;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Serilog;
using System.Threading;

namespace SlugChess.Services
{
    public class SlugChessService : INotifyPropertyChanged
    {
        #region init stuff
        public static SlugChessService Client => _client ?? throw new Exception("Asking for client when client is null");
        private static SlugChessService? _client = null;
        public static string Usertoken => Client?.UserData?.Usertoken ?? "";
        public static UserIdentification UserIdent => Client?.UserIdentification ?? new UserIdentification();

        private static string localhost_cert = """
-----BEGIN CERTIFICATE-----
MIIFLTCCAxWgAwIBAgIUQWcy4yReU9vvGzHfuFrpEMFzb+IwDQYJKoZIhvcNAQEL
BQAwGDEWMBQGA1UEAwwNc2x1Z2NoZXNzLmNvbTAeFw0yMzA0MDQxNzQyMDVaFw0z
MzA0MDExNzQyMDVaMBgxFjAUBgNVBAMMDXNsdWdjaGVzcy5jb20wggIiMA0GCSqG
SIb3DQEBAQUAA4ICDwAwggIKAoICAQCxcFXJW6BkTg2B+EhKsMf/QtKF0YdtX/UL
Wo/YtLCdZoVGOd5phqTg0N8A2ZzGEsZjm0FlN0vn2fGD2Q080Cb9SSgeXjEPg1rI
FBi3jLENUVKENPf/o+f2aG/Ibb5Za/nw6n8akQ7W2b5o+m/iSRJH9hRHFJHDfAcq
BpSJHELHvSoI+Ll/FrpUbdMo5XCHc2vxzGURfOgx48rnkb+Btzkl37O/TqAmr4kQ
q+d0xHp3og7RDbNU9VXXd6t6wxt5DGopGDwnXAY4xJA+Mih0aOw6+bFbN0OELzZr
SC1b2o2DYyPJphNrtqlKdt4E0S7fQS4W8nSd2EMGXMP83jdjbJOk1rmn3ajz9sYN
bWTlofFb49EuGJ9UrXbnoT1NmHvMLQVDYONHgbra3V8T62rv05n2jSeALAwZzNuk
Y5r44hkvxa9pWa1L/dhqt9dNwouYjbxXLTfkOos/0vLkyVmr4X+aeVy9PWMqW5TJ
SBnX10nKjPNi8r9gG1sl3ejU1CqomDF6eHRHrULXp8oT13Wi00MOc4obnV0f5Nes
YKvtRNvy87YMaIBwGEPqa9qSkRvcERxr/08lGND+V4gA0JoAP/sHtBgF5561zc+t
fJl9d867PpBamak4DcxmMt1DIwdLkTIhDCurYeUnTc/kbzFylQ9LWwY0my+MLL5T
pZcyyzai2QIDAQABo28wbTAdBgNVHQ4EFgQUcC1GwB6bEb8BDqFxPmo5or0dZEEw
HwYDVR0jBBgwFoAUcC1GwB6bEb8BDqFxPmo5or0dZEEwDwYDVR0TAQH/BAUwAwEB
/zAaBgNVHREEEzARgglsb2NhbGhvc3SHBH8AAAEwDQYJKoZIhvcNAQELBQADggIB
AEf0d/9k64HFyJqDpdTFIF7S6ChA4V/DMVTuBKEpkxGPE++SAgAxauAGVmibO638
JYNJcOrhhK1p7av8gf31uXp5FiQGQeQnfja0jG29hZ3+n0GLGf+BRdxwfKzQfmZP
COs1r1L/yvLXChJ/P8QuVT7bsjkFkc0g1kkFHI5TbUn2vZgENUf8/ZqrJ2Gl2YLL
UFA656NZ0gw6VCWUwial/7inkk+aNevNErIMsGffWp2+DKecU73pcL+HBCpbc4ne
IczMyhgkKI7TBfd8JaYejh0so0EbxAptwcM7SdhDVRAqYfi/XO+DMiwHLZ+PaEwO
29iK48WsXhEjQ8pVgXO7IGqKvWg97asNo1EUNK+fYm6Td7pKaj3hCgvPWrU/xhg/
CCWLQYkvjgvovkNjpRbQLQyiETd4FL04KPNnOFGfi3/MsZYHLCL02P4P/Euj5kJd
yjS9qGSOy1pvxc/7BJyCHe79KzfPUUISrF3dgbmkmWTFir4cD8DT1o7dCXjC7zit
wHpi1xYe2bNfmwy6gQB17ZJnE3TY/ormakf/wrauRs3fRTH12CqZX1bTOCabvTT5
jLq8HkWFqafsJBlj/iKrGwtXE5YwUKrKz1kHj+cmYRgErsaKWLnWJjlNGdw1CTsD
UsvMWM0q2xFVDA6mEnm3TCS2NTT9FW8ov8Bqsc3Kii3t
-----END CERTIFICATE-----
""";
    private static string _slugchess_com_cert = """
-----BEGIN CERTIFICATE-----
MIIFZTCCA02gAwIBAgIUcewnlPx+E77hrt2dtSv2jOHNZCUwDQYJKoZIhvcNAQEL
BQAwGDEWMBQGA1UEAwwNc2x1Z2NoZXNzLmNvbTAeFw0yMzA0MDQxNzQyMDNaFw0z
MzA0MDExNzQyMDNaMBgxFjAUBgNVBAMMDXNsdWdjaGVzcy5jb20wggIiMA0GCSqG
SIb3DQEBAQUAA4ICDwAwggIKAoICAQDWWeUx/OXee1+E6EtI8iUghLP3tfSoXIwB
25ugPmWRlEzTFha+MJB2x6eqscq4kFJINa4G0Eiuav9TeWitLSJRAsV0u0WgE9yM
sR/PIY0WEd5HCuxc5RLldgLtGesaOS7UZeMYXo7oa31oaaqrZ7l+EU8oKXZjSS2m
U3+Lc3TO4ck4AwPYATSc+KZNkLU3M/16fRGRBe+eXyemdBuTeI5tHznxJ9iGpDqL
gvtlb8K/tgtGHNEBge4TMNSZmtEXQ539mXgRGhynYwiJXrKrUW5J2Jeo7jAAjtM9
Ip70yxnn6qodE/f8Lva9P6HH7QgI50tk9y+scnJm22BEgRgfgnBqhajUhwTqJ/d7
IxTRC1j+a1oJJnkg/Lp8i6fYzH450cBak+9Wewvu+FrerrZSbYxgNVzvvBOcfwKx
DTkU93bZHWQBBPwlN8R4kYvqj3ZmaT/VYe1LZ+8xHttbfTdgsaj8ie0gctDEin96
j96g9AChxQSLy74PXU1b4XfVyFfTtHJmBSip+mFbJxKNrM6saulabRvVEsmcc51F
9LTIY1nkgCMOuqMEnAXQtg73tgHNEwHU85uQiaD1FAMOlvYwkIy2zYSP39YlVvuF
YaU3CmbAIQoEEHlKWfIVhxmXhCV9CuFiDvh+eMxfsp++VufSLQmBTDz33TLujegs
Q3VW4i4biQIDAQABo4GmMIGjMB0GA1UdDgQWBBRMXw5h4CaBwFtIeuGaqKcUbnFj
5jAfBgNVHSMEGDAWgBRMXw5h4CaBwFtIeuGaqKcUbnFj5jAPBgNVHRMBAf8EBTAD
AQH/MFAGA1UdEQRJMEeCDXNsdWdjaGVzcy5jb22CEnNydnIuc2x1Z2NoZXNzLmNv
bYIRZGJnLnNsdWdjaGVzcy5jb22CCWxvY2FsaG9zdIcEubU9JzANBgkqhkiG9w0B
AQsFAAOCAgEAcr0N9S7zm637L3scvRYBH5FT3ZQE640zM6z3ZBgBths8PdEQleMG
fAFwwAR1Phpo4yKStKdHZ6XQZt8yZkB40qJZUNXixOPLkuzU5K1CbkGtCx5BOEYH
HhHWwCO/3ujQC1gyc3Vh+VrWJo3003JInC8XB6/tG/B/fdai4pJsRp3Ll19OHmYn
WFwZDe9eGu+w/AGy4zIOQapk4x2vn+6txb8b3J4ix5DbXUbVSPbwcWzbqAQVyllk
Zb936RLiHuPkcTKfh5xIIzR6YhXpTRZfROeZF9XsBUQjdRDJzaqi0ozbwm1/ln41
Q2fPSgdUKnqufAc7LMw3sy33F+Bm6JBaHEYj+ai5Kpx8py+TeOkFOEBSpYbzbmJK
o9gsCAw56uXf09wUse6I8z6UHVzYmQH8lFN0MRpe37Ec7zUizbdEGp7/C1zYcKk3
740fa19A8SNfNA4ZesT3ZnRL2XRZ2apoB8GOVlGEIIDrg2wzwX77CCtYzxe1/Aep
55ZCO8FurE1oJKjRlcjTrvQk2zLziukvVXTg1BAttpTMBcd6oWCPGjqrTPkZkBjz
iVQHsfkZBqa0kgH+vBNbFFSCCVH5gVA04VteWMcp48b5KQvw9xEsoCa/SK2rzMGd
+oySQwZBsYYLZQAK4gKZrbV6LNIaeareAOcOowGcTqd2gDMoDkyOLyo=
-----END CERTIFICATE-----
""";
        public static void Instanciate(string adress, int port, bool use_tls)
        {
            if (_client != null) {
                Log.Warning("Allready initialized ChessCom Client. ");
                return;
            }
            
            _client = new SlugChessService(adress, port, use_tls);
        }
        public static void DeInstansiate()
        {
            throw new NotImplementedException();

        }
        //public object temp;
        private SlugChessService(string adress, int port, bool use_tls)
        {


            Log.Information("Connecting to: " + adress+":"+port);
            if(use_tls){
                var certs = _slugchess_com_cert;
#if DEBUG
                if(adress == "localhost"){
                    certs = localhost_cert;
                }
#endif
                if(File.Exists(Program.RootDir+"certs/server_cert.crt")){
                    Log.Information("Using custom root certificate: certs/server_cert.crt");
                    certs = File.ReadAllText(Program.RootDir+"certs/server_cert.crt");
                }
                Log.Information("   connection is TLS encrypted");
                var credentials = new Grpc.Core.SslCredentials(certs);
                _channel = new Channel(adress, port, credentials);
            } else {
                Log.Information("   connection is insecure");
                _channel = new Channel(adress, port, ChannelCredentials.Insecure);
            }
            _channel.ConnectAsync(DateTime.UtcNow + TimeSpan.FromSeconds(3)).ContinueWith(x =>
            {
                if (x.IsCompletedSuccessfully)
                {
                    ConnectionAlive = true;
                }
                else
                {
                    ServerConnectionFailed = true;
                }
            });
            Call = new ChessCom.ChessCom.ChessComClient(_channel);
            _heartbeatTimer.AutoReset = true;
            _heartbeatTimer.Elapsed += (o, e) => 
            {
                try
                {
                    if (!Call.Alive(new Heartbeat { Alive = true, UserIdent = SlugChessService.UserIdent}).Alive) 
                    { 
                        UserLoggedIn.OnNext(false); 
                        UserData = new UserData(); 
                    }

                }
                catch(Grpc.Core.RpcException ex)
                {
                    // EX here means Heartbeat failed for some reason.
                    #pragma warning disable CA2200
                    throw ex;
                    #pragma warning restore CA2200
                }
            };
            UserLoggedIn.Subscribe(loggedIn => { 
                if (loggedIn) { 
                    _heartbeatTimer.Start();
                    Task.Run(() =>
                    {
                        var stream = Call.ChatMessageListener(SlugChessService.UserIdent);
                        while (stream.ResponseStream.MoveNext().Result) // Gets inner exeption with status code 'Unavailable' when server closes with players logged in
                        {
                            //Send the message to UI thread to avoid weird race conditions. Rx should be single threaded.
                            Dispatcher.UIThread.InvokeAsync(() =>
                            {
                                MessageToLocal(stream.ResponseStream.Current.Message, stream.ResponseStream.Current.SenderUsername);
                            }).Wait(); //Wait to process one before starting to process the next
                        }
                        stream.Dispose();
                    });
                } else {
                    _heartbeatTimer.Stop();
                } 
            });
            UserLoggedIn.Subscribe(x => {Dispatcher.UIThread.InvokeAsync(() => UserLoggedIn_UiAsync.OnNext(x)); });

        }
        #endregion

        private Channel _channel;
        public ChessCom.ChessCom.ChessComClient Call { get; private set;} 

        public event PropertyChangedEventHandler? PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool ConnectionAlive
        {
            get {return _connectionAlive; }
            set {if (value != _connectionAlive){_connectionAlive = value; NotifyPropertyChanged();} }
        }
        private bool _connectionAlive = false;

        public bool ServerConnectionFailed
        {
            get {return _serverConnectionFailed; }
            set {if (value != _serverConnectionFailed){_serverConnectionFailed = value; NotifyPropertyChanged();} }
        }
        private bool _serverConnectionFailed = false;

        //public AvaloniaList<KeyValuePair<string, VisionRules>> ServerVisionRuleset { get; } = new AvaloniaList<KeyValuePair<string, VisionRules>>();
        public AvaloniaList<string> ServerNamedVariants { get; } = new AvaloniaList<string>();
        //private AvaloniaList<KeyValuePair<string, VisionRules>> _serverVisionRulesetTemp = new AvaloniaList<KeyValuePair<string, VisionRules>>();

        public BehaviorSubject<bool> UserLoggedIn { get; set; } = new BehaviorSubject<bool>(false);
        public BehaviorSubject<bool> UserLoggedIn_UiAsync { get; set; } = new BehaviorSubject<bool>(false);

        public void MessageToLocal(string message, string sender) => _messages.OnNext(
            DateTime.Now.ToString("HH:mm:ss") + " " + sender + ": " + message);
        public IObservable<string> Messages => _messages;
        private ReplaySubject<string> _messages = new ReplaySubject<string>();
        public UserData UserData
        {
            get => _userData;
            set { if (value != _userData) { _userData = value; NotifyPropertyChanged(); } }
        }
        private UserData _userData = new UserData();
        public UserIdentification UserIdentification
        { 
            get => _userIdentification;
            set { if (value != _userIdentification) { _userIdentification = value; NotifyPropertyChanged(); }}
        }
        private UserIdentification _userIdentification = new UserIdentification();
        public string Test
        {
            get { return _test; }
            set { if (value != _test) { _test = value; NotifyPropertyChanged(); } }
        }
        private string _test = "test sting";

        private System.Timers.Timer _heartbeatTimer = new System.Timers.Timer(60*1000);

        public void GetNewUserdata()
        {
            if(_client == null)return;
            var req = new UserDataRequest { UserIdent = UserIdentification, Username = UserData.Username };
            var ud = _client.Call.GetPublicUserdata(req);
            //WAAAAA. Make sure not to overwrite the usertoken
            UserData = new UserData { Username=UserData.Username, Usertoken = UserData.Usertoken, Elo = ud.Elo };
        }

        public Task<LoginResult> LoginInUserAsync(string username, string password) => Task.Run<LoginResult>(() => 
            {
                if(_client == null) return new LoginResult{ SuccessfullLogin=false, LoginMessage="Could not login as not connected to SlugChess server"};

                var ver = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
                try
                {
                    var result = _client.Call.Login(new LoginForm { Username = username, Password = password, MajorVersion = ver.FileMajorPart.ToString(), MinorVersion = ver.FileMinorPart.ToString(), BuildVersion = ver.FileBuildPart.ToString() });
                    if (result.SuccessfullLogin)
                    {
                        UserData = new UserData
                        {
                            Username = result.UserData.Username,
                            Usertoken = result.UserData.Usertoken,
                            Elo = result.UserData.Elo
                        };
                        UserIdentification = new UserIdentification
                        {
                            Usertoken = result.UserData.Usertoken,
                            Secret = result.Secret
                        };
                        UserLoggedIn.OnNext(true);
                        //MainWindowViewModel.SendNotification("Logged in as " + SlugChessService.Client.UserData?.Username ?? "non");
                        //Call.ServerVisionRulesetsAsync(new ChessCom.Void()).ResponseAsync.ContinueWith(y =>
                        //{
                        //    ServerVisionRuleset.Clear();
                        //    var a = y.Result.VisionRulesets.ToList();
                            //a.Insert(0, new KeyValuePair<string, VisionRules>("No Vision Rules", new VisionRules { Enabled = false }));
                        //    ServerVisionRuleset.AddRange(a);
                        //});
                        Call.GetNamedVariantsAsync(new ChessCom.Void()).ResponseAsync.ContinueWith(y =>
                        {
                            ServerNamedVariants.Clear();
                            ServerNamedVariants.AddRange(y.Result.Variants);
                        });

                        return result;
                    }
                    else
                    {
                        Log.Information("Login failed. " + result.LoginMessage);
                        MainWindowViewModel.SendNotification("Login attempt rejected from server");
                        return result;
                    }
                }
                catch(RpcException ex)
                {
                    if(ex.StatusCode == StatusCode.Unavailable)
                    {
                        Log.Error("SlugChess Server Unavailable. " + ex.Message);
                        MainWindowViewModel.SendNotification("SlugChessServer Unavailable");
                        return new LoginResult
                        {
                            SuccessfullLogin = false,
                            LoginMessage = "SlugChess Server unavailable. The server might be down or your internet connection interupted\n" +
                            "Press login button to try again or complain to the SlugChess Server admin at admin@spaceslug.no"
                        };
                    }   
                    else 
                    {
                        #pragma warning disable CA2200
                        throw ex;
                        #pragma warning restore CA2200
                    }
                }
                
            });
        public Task Logout() => Task.Run(() =>
        {
            if (ConnectionAlive)
            {
                try {
                    var result = Call.Logout(SlugChessService.UserIdent);
                    if (!result.SuccessfullLogout)
                    {
                        MainWindowViewModel.SendNotification("Malformed logout: " + result.LogoutMessage);
                    }
                    if(result.LogoutMessage != "") Log.Information("Logout message: "+result.LogoutMessage);
                    
                }
                catch(RpcException ex){
                    MainWindowViewModel.SendNotification("Malformed logout: " + ex.Message);
                    Log.Warning("Malformed logout. Got RpcException " + ex.Message);
                }

                UserLoggedIn.OnNext(false);
                MainWindowViewModel.SendNotification("Logged out");
                UserData = new UserData();
            }
        });

        public IObservable<MoveResult> GetMatchListener(string matchId)
        {
            var subject = new Subject<MoveResult>();
            Task.Run(() =>
            {
                AsyncServerStreamingCall<MoveResult>? stream = null;
                try {
                    stream = Call.MatchEventListener(new MatchObserver { MatchId = matchId, UserIdent = UserIdent});
                    while (stream.ResponseStream.MoveNext().Result)
                    {
                        subject.OnNext(stream.ResponseStream.Current);
                    }
                } catch(Exception ex){
                    Log.Error("MatchListener streaming exception: " + ex.Message);
                    subject.OnError(ex);
                } finally {
                    stream?.Dispose();
                    subject.OnCompleted();
                }
            });
            return subject;
        }

        /// IObservable is called as Dispatcher.UIThread.InvokeAsync
        /// IObservable is a ReplaySubject
        public static IObservable<T> GetStreamListener<T>(AsyncServerStreamingCall<T> stream, CancellationToken cancelToken)
        {
            var subject = new ReplaySubject<T>();
            Task.Run(() =>
            {
                try 
                {
                    while (stream.ResponseStream.MoveNext(cancelToken).Result)
                    {
                        subject.OnNext(stream.ResponseStream.Current);
                    }
                }
                catch (System.AggregateException ex)
                {
                    if(cancelToken.IsCancellationRequested){
                        Log.Debug($"AggregateException in GetStreamListener. But cancelation was requested from client. Completing stream. Message: {ex.Message} Message Inner: {ex.InnerException?.Message}");    
                    } else {
                        Log.Error($"AggregateException in GetStreamListener. Message: {ex.Message} Message Inner: {ex.InnerException?.Message}");
                        subject.OnError(ex);
                    }

                }
                catch (RpcException ex)
                {
                    Log.Error($"RpcException in GetStreamListener. Status: {ex.Status}. Message: {ex.Message}");
                    subject.OnError(ex);
                }
                catch(InvalidOperationException ex){
                    Log.Error($"Exception in GetStreamListener. Message: {ex.Message}");
                    subject.OnError(ex);
                }
                finally
                {
                    stream.Dispose();
                    subject.OnCompleted();
                }
            });
            return subject;
        }

    }
        
}
