﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using Avalonia.Svg.Skia;
using Avalonia.Threading;

namespace SlugChess.Services
{
    public static class AssetBank
    {
        private static readonly string _assetFolder = "Assets/";
        private static readonly string _builtInAssetFolder = "avares://SlugChess/Assets/";

        public static IImage GetImage(string s) => _loadedImage[s];
        private static readonly Dictionary<string, IImage> _loadedImage = new Dictionary<string, IImage>();

        /// <summary>
        /// svg vil naturaly overwrite png files.
        /// </summary>
        /// <returns></returns>
        public static Task<bool> LoadAssets() => Task.Run<bool>(()=>
        {
            foreach (var assetUri in AssetLoader.GetAssets(new Uri(_builtInAssetFolder), null))
            {
                if (assetUri.AbsolutePath.ToLower().EndsWith(".png"))
                {
                    _loadedImage[Path.GetFileNameWithoutExtension(assetUri.AbsolutePath)] = new Bitmap(AssetLoader.Open(assetUri));
                    Serilog.Log.Debug(Path.GetFileNameWithoutExtension(assetUri.AbsolutePath) + " png");
                }
            }
            var path = Program.RootDir + _assetFolder;
            if (Directory.Exists(path))
            {
                foreach (var file in Directory.EnumerateFiles(path,"*.*", SearchOption.AllDirectories))
                {
                    if (file.ToLower().EndsWith(".svg"))
                    {
                        var svg = new SvgSource();
                        var picture = svg.Load(file);
                        Dispatcher.UIThread.InvokeAsync(() => {
                            _loadedImage[Path.GetFileNameWithoutExtension(file)] = new SvgImage { Source=svg };
                            Serilog.Log.Debug(Path.GetFileNameWithoutExtension(file) + " svg");
                        }).Wait(); // Waiting for return might not be nessesary. Need more testing
                    }
                    else if (file.ToLower().EndsWith(".png"))
                    {
                        _loadedImage[Path.GetFileNameWithoutExtension(file)] = new Bitmap(file);
                        Serilog.Log.Debug(Path.GetFileNameWithoutExtension(file) + " png");
                    }
                }
            }
            
            return true;
        });

        public static IImage EmptyImage => AssetBank.GetImage("empty");
        public static IImage WhitePawnImage => _loadedImage["whitePawn"];
        public static IImage? ImageFromPiece(ChessCom.Pieces piece) =>
            piece switch
            {
                ChessCom.Pieces.None => AssetBank.GetImage("empty"),
                ChessCom.Pieces.BlackKing => AssetBank.GetImage("blackKing"),
                ChessCom.Pieces.BlackQueen => AssetBank.GetImage("blackQueen"),
                ChessCom.Pieces.BlackRook => AssetBank.GetImage("blackRook"),
                ChessCom.Pieces.BlackBishop => AssetBank.GetImage("blackBishop"),
                ChessCom.Pieces.BlackKnight => AssetBank.GetImage("blackKnight"),
                ChessCom.Pieces.BlackPawn => AssetBank.GetImage("blackPawn"),
                ChessCom.Pieces.WhiteKing => AssetBank.GetImage("whiteKing"),
                ChessCom.Pieces.WhiteQueen => AssetBank.GetImage("whiteQueen"),
                ChessCom.Pieces.WhiteRook => AssetBank.GetImage("whiteRook"),
                ChessCom.Pieces.WhiteBishop => AssetBank.GetImage("whiteBishop"),
                ChessCom.Pieces.WhiteKnight => AssetBank.GetImage("whiteKnight"),
                ChessCom.Pieces.WhitePawn => AssetBank.GetImage("whitePawn"),
                _ => throw new ArgumentException(message: "invalid enum value", paramName: nameof(piece)),
            };
    }
}
