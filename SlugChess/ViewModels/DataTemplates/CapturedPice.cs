﻿using Avalonia.Media;
using Avalonia.Media.Imaging;
using Google.Protobuf.WellKnownTypes;
using SlugChess.Models;
using SlugChess.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlugChess.ViewModels.DataTemplates
{
    public class CapturedPiece
    {
        public SolidColorBrush Background { get; }
        public IImage? Piece { get; }
        public string Field { get; }
        public ChessCom.Pieces PieceType { get; }
        public CapturedPiece(ChessCom.PieceCapture pc)
        {
            if( pc.Piece == ChessCom.Pieces.WhiteBishop || pc.Piece == ChessCom.Pieces.BlackBishop){
                Background = (ChessboardModel.FieldColorLight(pc.Location) ? StyleMirror.WhiteField : StyleMirror.BlackField) ?? new SolidColorBrush(Colors.Pink);
            }else{
                Background = StyleMirror.ThemeControlMidBrush ?? new SolidColorBrush(Colors.Pink);
            }
            Piece = AssetBank.ImageFromPiece(pc.Piece);
            Field = pc.Location;
            PieceType = pc.Piece;
        }
        private CapturedPiece(SolidColorBrush b, Bitmap? p, string f)
        {
            Background = b;
            Piece = p;
            Field = f;
            PieceType = ChessCom.Pieces.None;
        }

        public static CapturedPiece Empty => new CapturedPiece(new SolidColorBrush { Opacity=0}, null, "");

        public static IEnumerable<CapturedPiece> CapturedPieces => new List<CapturedPiece> { 
            new CapturedPiece(new ChessCom.PieceCapture { Location="a4", Piece=ChessCom.Pieces.WhitePawn}),
            new CapturedPiece(new ChessCom.PieceCapture { Location="b4", Piece=ChessCom.Pieces.BlackBishop}),
            new CapturedPiece(new ChessCom.PieceCapture { Location="c4", Piece=ChessCom.Pieces.BlackPawn}),
            new CapturedPiece(new ChessCom.PieceCapture { Location="e4", Piece=ChessCom.Pieces.WhiteQueen}),
            new CapturedPiece(new ChessCom.PieceCapture { Location="f4", Piece=ChessCom.Pieces.WhiteKnight}),
            new CapturedPiece(new ChessCom.PieceCapture { Location="h4", Piece=ChessCom.Pieces.BlackRook})
        };
    }
}
