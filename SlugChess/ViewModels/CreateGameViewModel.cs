﻿using Avalonia.Collections;
using ChessCom;
using DynamicData.Binding;
using ReactiveUI;
using SlugChess.Services;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive;
using System.Reactive.Subjects;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Threading;
using Serilog;

namespace SlugChess.ViewModels
{
    #pragma warning disable 8612
    public class CreateGameViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
    #pragma warning restore 8612
    {
        public const int DEFAULT_START_TIME_MIN = 5;
        public const int DEFAULT_EXTRA_TIME_SEC = 6;
        public const int DEFAULT_CHESSTYPE_INDEX = 2;
        public const int DEFAULT_VISION_RULES_INDEX = 0;
        public const int DEFAULT_HOST_COLOR_INDEX = 2;
        public const bool DEFAULT_ALLOW_OBSERVERS = false;


        public ViewModelActivator Activator { get; } = new ViewModelActivator();
        public string UrlPathSegment => "/creategame";
        public IScreen HostScreen { get; }

        public ICommand Cancel => ((MainWindowViewModel)HostScreen).NavigateBack;

        public ReactiveCommand<Unit, Unit> HostGame { get; }

        public string StartTimeMin
        {
            get => _startTimeMin.ToString();
            set => this.RaiseAndSetIfChanged(ref _startTimeMin, int.TryParse(value, out int n) ? n : 0);
        }
        private int _startTimeMin = DEFAULT_START_TIME_MIN;

        public string ExtraTimeSec
        {
            get => _extraTimeSec.ToString();
            set => this.RaiseAndSetIfChanged(ref _extraTimeSec, int.TryParse(value, out int n) ? n : 0);
        }
        private int _extraTimeSec = DEFAULT_EXTRA_TIME_SEC;
        public int ChessTypeSelectedIndex
        {
            get => _chessTypeSelectedIndex;
            set => this.RaiseAndSetIfChanged(ref _chessTypeSelectedIndex, value);
        }
        private int _chessTypeSelectedIndex = DEFAULT_CHESSTYPE_INDEX;

        public int HostColorSelectedIndex
        {
            get => _hostColorSelectedIndex;
            set => this.RaiseAndSetIfChanged(ref _hostColorSelectedIndex, value);
        }
        private int _hostColorSelectedIndex = DEFAULT_HOST_COLOR_INDEX;

        public int VisionRulesSelectedIndex
        {
            get => _visionRulesSelectedIndex;
            set => this.RaiseAndSetIfChanged(ref _visionRulesSelectedIndex, value);
        }
        private int _visionRulesSelectedIndex = DEFAULT_VISION_RULES_INDEX;

        public bool AllowObservers
        {
            get => _allowObservers;
            set => this.RaiseAndSetIfChanged(ref _allowObservers, value);
        }
        private bool _allowObservers = DEFAULT_ALLOW_OBSERVERS;

        //public IList<KeyValuePair<string, VisionRules>> VisionRuleItems => SlugChessService.Client.ServerVisionRuleset.OrderByDescending(x => x.Key).ToList();
        public IList<string> VariantsItems => SlugChessService.Client.ServerNamedVariants.OrderByDescending(x => x).ToList(); // Will include custom when supported on server



        private Subject<(IObservable<LobbyState>, CancellationTokenSource)> _lobbyState = new Subject<(IObservable<LobbyState>, CancellationTokenSource)>();
        public IObservable<(IObservable<LobbyState>, CancellationTokenSource)> LobbyState => _lobbyState;

        public CreateGameViewModel(IScreen? screen = null)
        {
            HostScreen = screen ?? Locator.Current?.GetService<IScreen>() ?? throw new InvalidProgramException("Locator is null");
            
            HostGame = ReactiveCommand.CreateFromTask(_ => HostGameRequest());


            this.WhenActivated(disposables =>
            {
                Disposable.Create(() =>
                {
                    
                }).DisposeWith(disposables);
            });


        }

        private Task HostGameRequest() => Task.Run(() =>
        {

           //try
           //{
            var starttime = _startTimeMin;
            var movetime = _extraTimeSec;
            ChessCom.ChessType chessType = _chessTypeSelectedIndex == 0 ? ChessCom.ChessType.Classic :
                                            _chessTypeSelectedIndex == 1 ? ChessCom.ChessType.FisherRandom :
                                            ChessCom.ChessType.SlugRandom;
            ChessCom.SideType sideType = _hostColorSelectedIndex == 0 ? ChessCom.SideType.HostIsWhite :
                                            _hostColorSelectedIndex == 1 ? ChessCom.SideType.HostIsBlack :
                                            ChessCom.SideType.Random;
            // will be used as part of custom game
            //string vrType = VisionRuleItems[_visionRulesSelectedIndex].Key;



            var cts = new CancellationTokenSource();
            ((MainWindowViewModel)HostScreen).Notification = "Hosting game";

                var stream = SlugChessService.Client.Call.HostGame(new ChessCom.HostGameRequest
                {
                    
                    UserIdent = SlugChessService.UserIdent,
                    HostedGame = new ChessCom.HostedGame {
                        GameRules = new ChessCom.GameRules
                        {
                            Named = VariantsItems[_visionRulesSelectedIndex],
                            //ChessType = chessType,
                            SideType = sideType,
                            //TypeRules = vrType,
                            TimeRules = new ChessCom.TimeRules { PlayerTime = new ChessCom.Time { Minutes = starttime }, SecondsPerMove = movetime }
                        },
                        Host = SlugChessService.Client.UserData,
                        ObserversAllowed = AllowObservers
                    }
                }, null, null, cts.Token);

                cts.Token.Register(() => {
                    Log.Information("Token match listener cancled");
                });

                _lobbyState.OnNext((SlugChessService.GetStreamListener(stream, cts.Token), cts));

                
                return;
       });
    }
}
