﻿using Avalonia.Controls;
using Avalonia.Threading;
using ChessCom;
using ReactiveUI;
using SlugChess.Models;
using SlugChess.Services;
using Splat;
using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Serilog;

namespace SlugChess.ViewModels
{
    #pragma warning disable 8612
    public class StartMenuViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
    #pragma warning restore 8612
    {
        public string UrlPathSegment => "/startmenu";
        public IScreen HostScreen { get; }

        public ViewModelActivator Activator { get; } = new ViewModelActivator();

        public string Test
        {
            get => _test;
            set => this.RaiseAndSetIfChanged(ref _test, value);
        }
        private string _test = "...";

        public bool ServerConnectionFailed
        {
            get => _serverConnectionFailed;
            set => this.RaiseAndSetIfChanged(ref _serverConnectionFailed, value);
        }
        private bool _serverConnectionFailed = false;

        public SlugChessService SlugChess => SlugChessService.Client;

        public ICommand MoveToLogin => _moveToLogin;
        private readonly ReactiveCommand<(string?, string?), (string?, string?)> _moveToLogin;
        public ICommand MoveToRegister => _moveToRegister;
        private readonly ReactiveCommand<Unit, Unit> _moveToRegister;
        public ICommand MoveToHostGame => _moveToHostGame;
        private readonly ReactiveCommand<Unit, Unit> _moveToHostGame;
        private CreateGameViewModel _vmHostGame { get; }

        public ICommand MoveToJoinGame => _moveToJoinGame;
        private readonly ReactiveCommand<Unit, Unit> _moveToJoinGame;
        private GameBrowserViewModel _vmJoinGame { get; }

        public ICommand MoveToViewPgnReplay => _viewPgnReplay;
        private readonly ReactiveCommand<Window, Replay?> _viewPgnReplay;

        public ICommand MoveToServerPgnSelect => _moveToServerPgnSelect;
        private readonly ReactiveCommand<Unit, Unit> _moveToServerPgnSelect;
        private ServerPgnSelectViewModel _vmServerPgnSelect { get; }

        public ICommand Logoff => _logoff;
        private readonly ReactiveCommand<Unit, Unit> _logoff;

        public ICommand ShowHelp => _showHelp;
        private readonly ReactiveCommand<Unit, Unit> _showHelp;
        public ICommand ShowLicense => _showLicense;
        private readonly ReactiveCommand<Unit, Unit> _showLicense;
        public ICommand ShowSourceCode => _showSourceCode;
        private readonly ReactiveCommand<Unit, Unit> _showSourceCode;

        public ICommand Exit => ((MainWindowViewModel)HostScreen).Exit;

        private bool _haveTriedDebugLogin = false;

        public string SlugChessVersion => $"SlugChess v{Program.GetSlugChessVersion()}";

        public string LicenseText => """
SlugChess, Copyright (C) 2020-2022 Spaceslug
SlugChess comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions; click 'See License' for details.    
""";

        public StartMenuViewModel(IScreen? screen = null)
        {
            HostScreen = screen ?? Locator.Current?.GetService<IScreen>() ?? throw new InvalidProgramException("Locator is null");
            SlugChess.UserLoggedIn.Subscribe(x => {
                Test = x.ToString();
            });

            _vmJoinGame = new GameBrowserViewModel(HostScreen);
            _vmHostGame = new CreateGameViewModel(HostScreen);
            _vmServerPgnSelect = new ServerPgnSelectViewModel(HostScreen);
            var userLoggedIn = SlugChess.UserLoggedIn_UiAsync;
            _moveToHostGame = ReactiveCommand.Create(
                () => { HostScreen.Router.Navigate.Execute(_vmHostGame).Subscribe(); },
                userLoggedIn);
            _moveToJoinGame = ReactiveCommand.Create(
                () => { HostScreen.Router.Navigate.Execute(_vmJoinGame).Subscribe(); },
                userLoggedIn);
            Observable.Merge(
                _vmJoinGame.LobbyState, _vmHostGame.LobbyState)
                .Subscribe
                (((IObservable<LobbyState> o, CancellationTokenSource c) x) => { 
                    x.o.Subscribe(x => {}, x => {
                        Log.Error($"Lobby event onError {x.Message}");
                        if(HostScreen.Router.CurrentViewModel.Take(1).Wait() is LobbyViewModel) {
                            Log.Debug("LobbyViewModel navigating back");
                            Dispatcher.UIThread.InvokeAsync(() => {
                                HostScreen.Router.NavigateBack.Execute().Subscribe();
                            });
                        }
                    });
                    Dispatcher.UIThread.InvokeAsync(() => {
                        HostScreen.Router.NavigateBack.Execute().Subscribe();
                        HostScreen.Router.Navigate.Execute(new LobbyViewModel(x.o, x.c, HostScreen)).Subscribe();
                    });
                });
            _viewPgnReplay = ReactiveCommand.CreateFromObservable<Window, Replay?>(PlayViewModel.ViewPgnReplayImpl, userLoggedIn);
            _viewPgnReplay.Subscribe(x => {
                if(x != null){
                    MainWindowViewModel.SendNotification("Replay of " + x.White + " vs " + x.Black);
                    ShellHelper.PlaySoundFile(Program.RootDir + "Assets/sounds/match_start.wav");
                    var replayModel = new ReplayModel(x);
                    HostScreen.Router.Navigate.Execute(new PlayViewModel(new MatchModel(replayModel), replayModel)).Subscribe();
                }else{
                    MainWindowViewModel.SendNotification("Failed to load replay");
                }
                
            });
            _moveToServerPgnSelect = ReactiveCommand.Create(
                () => { HostScreen.Router.Navigate.Execute(_vmServerPgnSelect).Subscribe(); },
                userLoggedIn);
            _vmServerPgnSelect.ReplaySelected.Subscribe(x => {
                HostScreen.Router.NavigateBack.Execute().Subscribe();
                MainWindowViewModel.SendNotification("Replay of " + x.White + " vs " + x.Black);
                ShellHelper.PlaySoundFile(Program.RootDir + "Assets/sounds/match_start.wav");
                var replayModel = new ReplayModel(x);
                HostScreen.Router.Navigate.Execute(new PlayViewModel(new MatchModel(replayModel), replayModel)).Subscribe();
            });

            var canMoveToLogin = SlugChessService.Client.WhenAnyValue(x => x.ConnectionAlive);
            _moveToLogin = ReactiveCommand.Create(
                ((string? a, string? b) c) => c,
                canMoveToLogin);
            _moveToLogin.Subscribe(((string? a, string? b) c) =>
            {
                HostScreen.Router.Navigate.Execute(new LoginViewModel(null, c.a, c.b)).Subscribe();
            });
            _moveToRegister = ReactiveCommand.Create(
                () => { HostScreen.Router.Navigate.Execute(new RegisterUserViewModel()).Subscribe(); },
                canMoveToLogin);
            _logoff = ReactiveCommand.Create(
                () => { SlugChessService.Client.Logout(); }, 
                userLoggedIn
            );
            _showHelp = ReactiveCommand.Create(
                () => {
                    const string url = "https://gitlab.com/slugchess/slugchess/-/blob/main/slugchess-rules.md";
                    Task.Run(()=>{
                        if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                            ShellHelper.PowerShell($"Start-Process \"{url}\"");
                        } else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
                            ShellHelper.Bash($"xdg-open {url}");
                        }
                     });
                }
            );
            _showLicense = ReactiveCommand.Create(
                () => {
                    string url = $"{Program.RootDir}LICENSE.txt";
                    Task.Run(()=>{
                        if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                            ShellHelper.PowerShell($"Start-Process \"{url}\"");
                        } else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
                            ShellHelper.Bash($"xdg-open {url}");
                        }
                     });
                }
            );
            _showSourceCode = ReactiveCommand.Create(
                () => {
                    const string url = "https://gitlab.com/slugchess/slugchess/";
                    Task.Run(()=>{
                        if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                            ShellHelper.PowerShell($"Start-Process \"{url}\"");
                        } else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
                            ShellHelper.Bash($"xdg-open {url}");
                        }
                     });

                }
            );

            this.WhenActivated(disposables =>
            {
#if DEBUG
                if (Program.LaunchedWithParam("--debugLogin") && !_haveTriedDebugLogin)
                {
                    _haveTriedDebugLogin = true;
                    SlugChessService.Client.WhenAnyValue(x => x.ConnectionAlive).Where(conAlive => conAlive).Delay(TimeSpan.FromSeconds(0.2)).Subscribe(x =>
                    {
                        Dispatcher.UIThread.InvokeAsync(() => MoveToLogin.Execute((Program.GetParamValue("--debugLogin"), "dbg_passW0rd")));
                    }).DisposeWith(disposables);

                }
#endif
                Disposable.Create(() =>
                {

                }).DisposeWith(disposables);
            });
        }
    }
}
