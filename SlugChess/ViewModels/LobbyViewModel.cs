﻿using ChessCom;
using DynamicData;
using ReactiveUI;
using SlugChess.Models;
using SlugChess.Services;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Subjects;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Threading;
using Serilog;

namespace SlugChess.ViewModels
{
    public class LobbyViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
    {
        
        public string UrlPathSegment => "/lobby";
        public IScreen HostScreen { get; }

        public ViewModelActivator Activator { get; }

        public ChessCom.UserData? Host
        {
            get => _host;
            set => this.RaiseAndSetIfChanged(ref _host, value);
        }
        private ChessCom.UserData? _host = null;

        public ChessCom.UserData? Oppenent
        { 
            get => _oppenent;
            set => this.RaiseAndSetIfChanged(ref _oppenent, value);
        }
        private ChessCom.UserData? _oppenent = null;

        public ChessCom.UserData? ObserverSelected
        { 
            get => _observerSelected;
            set => this.RaiseAndSetIfChanged(ref _observerSelected, value);
        }
        private ChessCom.UserData? _observerSelected = null;
        public List<ChessCom.UserData> Observers
        {
            get => _observers;
            set => this.RaiseAndSetIfChanged(ref _observers, value);
        }
        private List<ChessCom.UserData> _observers = new List<UserData>{};

        public string ObserversText
        {
            get => _observersText;
            set => this.RaiseAndSetIfChanged(ref _observersText, value);
        }
        private string _observersText = "";
        public string MatchRulesText
        {
            get => _matchRulesText;
            set => this.RaiseAndSetIfChanged(ref _matchRulesText, value);
        }
        private string _matchRulesText = "";

        public string HostColorText
        {
            get => _hostColorText;
            set => this.RaiseAndSetIfChanged(ref _hostColorText, value);
        }
        private string _hostColorText = "";

        public string OpponentColorText
        {
            get => _opponentColorText;
            set => this.RaiseAndSetIfChanged(ref _opponentColorText, value);
        }
        private string _opponentColorText = "";

        public ICommand Kick => _kick;
        private readonly ReactiveCommand<UserData, Unit> _kick;

        public ICommand KickOppenent => _kickOpponent;
        private readonly ReactiveCommand<Unit, Unit> _kickOpponent;
        public ICommand StartGame => _startGame;
        private readonly ReactiveCommand<Unit, Unit> _startGame;

        private IObservable<LobbyState> _lobbyState;
        private Subject<HostedGame> _hostedGame = new Subject<HostedGame>();

        public IObservable<bool> IsHost {get; }
        

        public LobbyViewModel(IObservable<LobbyState> obsLobbyState, CancellationTokenSource cancellationTokenSource, IScreen? screen = null)
        {
            HostScreen = screen ?? Locator.Current?.GetService<IScreen>() ?? throw new InvalidProgramException("Locator is null");
            Activator = new ViewModelActivator();
            _lobbyState = obsLobbyState;
            var lobbyState = _lobbyState.Take(1).Wait();
            MatchRulesText =  GameRulesToText(lobbyState.HostedGameUpdate.GameRules, lobbyState.HostedGameUpdate.ObserversAllowed);
            _lobbyState.Subscribe(x => {
                Dispatcher.UIThread.InvokeAsync(() => {
                    switch (x.EventCase)
                    {
                        case LobbyState.EventOneofCase.HostedGameUpdate:
                            var hgu = x.HostedGameUpdate;
                            Host = hgu.Host; 
                            Oppenent = hgu.Joiner;
                            Observers = new List<UserData>(hgu.Observers);
                        break;
                        case LobbyState.EventOneofCase.LobbyClosed:
                            MainWindowViewModel.SendNotification($"Game Lobby closed");
                            Host = null;
                            Oppenent = null;
                            ObserversText = "";
                            MatchRulesText = "";
                        break;
                    }
                });
            });
             _lobbyState.Where(x => x.EventCase == LobbyState.EventOneofCase.HostedGameUpdate).Subscribe(x => {
                Dispatcher.UIThread.InvokeAsync(() => { _hostedGame.OnNext(x.HostedGameUpdate); });
            });
            IObservable<HostedGame> hostedGameUpdates = _hostedGame;
            IsHost = _hostedGame.Select(x => x.Host.Usertoken == SlugChessService.Usertoken);
            _kickOpponent = ReactiveCommand.Create(
                () => {
                    Kick.Execute(Oppenent);
                }, 
                hostedGameUpdates.Select(x => x.Host.Usertoken == SlugChessService.Usertoken && x.Joiner != null));

            _kick = ReactiveCommand.Create<UserData>(
                x => {
                    SlugChessService.Client.Call.LobbyCommand(new LobbyCommandMessage{UserIdent=SlugChessService.UserIdent,KickPlayerUsertoken=x.Usertoken});
                }, 
                //this.WhenAnyValue(x => x.ObserverSelected).Select(x => x != null));
                Observable.CombineLatest(this.WhenAnyValue(x => x.ObserverSelected), hostedGameUpdates, 
                (x, y) => x != null && y.Host.Usertoken == SlugChessService.Usertoken));

            _startGame = ReactiveCommand.Create(
                () => {
                    SlugChessService.Client.Call.LobbyCommand(new LobbyCommandMessage{UserIdent=SlugChessService.UserIdent,StartGame=new ChessCom.Void{}});
                }, 
                //this.WhenAnyValue(x => x.Oppenent).Select(x => x != null && x.Usertoken != SlugChessService.Usertoken)
                hostedGameUpdates.Select(x => x.Host.Usertoken == SlugChessService.Usertoken && x.Joiner != null)
            );

            this.WhenActivated(disposables =>
            {
                // Use WhenActivated to execute logic
                // when the view model gets activated.
                this.HandleActivation();

                if(cancellationTokenSource.IsCancellationRequested) {
                    Log.Information("LobbyView Activated canselation token allready canceled. Going back");
                    HostScreen.Router.NavigateBack.Execute().Subscribe();
                }
                
                disposables.Add(_lobbyState.Subscribe((x) => {
                    if (x.EventCase == LobbyState.EventOneofCase.MatchResult) {
                        Dispatcher.UIThread.InvokeAsync(() => {
                            HostScreen.Router.NavigateBack.Execute().Subscribe();
                            var matchModel = PlayViewModel.BootUpMatch(x.MatchResult); 
                            HostScreen.Router.Navigate.Execute(new PlayViewModel(matchModel)).Subscribe();
                        });
                    } else if(x.EventCase == LobbyState.EventOneofCase.LobbyClosed){
                        Log.Debug($"LobbyClosed {x.LobbyClosed}");
                        Dispatcher.UIThread.InvokeAsync(() => {
                            HostScreen.Router.NavigateBack.Execute().Subscribe();
                        });
                    }
                }));  

                // Or use WhenActivated to execute logic
                // when the view model gets deactivated.
                Disposable.Create(() => this.HandleDeactivation(cancellationTokenSource)).DisposeWith(disposables);
            });
        }

        private static string GameRulesToText(ChessCom.GameRules rules, bool observersAllowed){
            return (rules.VariantCase==GameRules.VariantOneofCase.Named? $"Variant: {rules.Named}": "Custom TODO") +
                $"\nTime Rules: {MatchInfoModel.TimeRulesToString(rules.TimeRules)}" +
                $"\nObservers Allowed: {observersAllowed}";
        }

        private void HandleActivation() 
        {
               
        }

        private void HandleDeactivation(CancellationTokenSource cancellationTokenSource) 
        {
            cancellationTokenSource.Cancel();
        }

    }
}
