﻿using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Threading;
using ChessCom;
using DynamicData;
using ReactiveUI;
using SlugChess.ViewModels.DataTemplates;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;

namespace SlugChess.ViewModels
{
    public class CapturedPiecesViewModel : ViewModelBase, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; } = new ViewModelActivator();

        public List<CapturedPiece> Items
        {
            get => _items;
            // If list becomes empty it stops working for some fucked up reasons
            set => this.RaiseAndSetIfChanged(ref _items, value);
        }
        private List<CapturedPiece> _items = new List<CapturedPiece>();

        public double PanelWidth
        {
            get => _panelWidth;
            set => this.RaiseAndSetIfChanged(ref _panelWidth, value);
        }
        private double _panelWidth;



        public CapturedPiecesViewModel()
        {
            this.WhenActivated(disposables =>
            {
                Disposable.Create(() => 
                {  
                    
                }).DisposeWith(disposables);
            });
        }
    }
}
