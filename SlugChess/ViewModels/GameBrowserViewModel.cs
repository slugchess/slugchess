﻿using ChessCom;
using DynamicData;
using ReactiveUI;
using SlugChess.Models;
using SlugChess.Services;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Subjects;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Threading;

namespace SlugChess.ViewModels
{
    #pragma warning disable 8612
    public class GameBrowserViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
    #pragma warning restore 8612
    {
        
        public string UrlPathSegment => "/gamebrowser";
        public IScreen HostScreen { get; }

        public ViewModelActivator Activator { get; }



        public ICommand Cancel => ((MainWindowViewModel)HostScreen).NavigateBack;

        public ReactiveCommand<Unit, HostedGamesMap> RefreshGamesList { get; }
        public ReactiveCommand<bool, Unit> JoinGame { get; }

        public ObservableCollection<MatchInfoModel> MatchInfoModels { get; }
        public MatchInfoModel? SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }
        private MatchInfoModel? _selectedItem;

        //public int SelectedIndex
        //{
        //    get => _selectedIndex;
        //    set => this.RaiseAndSetIfChanged(ref _selectedIndex, value);
        //}
        //private int _selectedIndex = -1;

        private Subject<(IObservable<LobbyState>, CancellationTokenSource)> _lobbyState = new Subject<(IObservable<LobbyState>, CancellationTokenSource)>();
        public IObservable<(IObservable<LobbyState>, CancellationTokenSource)> LobbyState => _lobbyState;

        public GameBrowserViewModel(IScreen? screen = null)
        {
            HostScreen = screen ?? Locator.Current?.GetService<IScreen>() ?? throw new InvalidProgramException("Locator is null");
            Activator = new ViewModelActivator();
#if DEBUG
            MatchInfoModels = new ObservableCollection<MatchInfoModel>(MatchInfoModel.FromTestData());
#else
            MatchInfoModels = new ObservableCollection<MatchInfoModel>();
#endif

            RefreshGamesList = ReactiveCommand.CreateFromTask(() => 
                {
                    return SlugChessService.Client.Call.AvailableGamesAsync(SlugChessService.UserIdent).ResponseAsync;
                }
            );
            RefreshGamesList.Subscribe(x => { MatchInfoModels.Clear(); MatchInfoModels.AddRange(MatchInfoModel.FromChesscom(x)); });
            var canJoin = this.WhenAnyValue(x => x.SelectedItem).Select(o => o != null);

            JoinGame = ReactiveCommand.CreateFromTask((bool beObserver) =>JoinGameRequest(beObserver),
                canJoin);
            //JoinGame.Subscribe((result) => {
            //    if (result.Succes)
            //    {

            //    }
            //});
            this.WhenActivated(disposables =>
            {
                // Use WhenActivated to execute logic
                // when the view model gets activated.
                this.HandleActivation();

                // Or use WhenActivated to execute logic
                // when the view model gets deactivated.
                Disposable.Create(() => this.HandleDeactivation()).DisposeWith(disposables);
            });
        }

        private Task JoinGameRequest(bool beObserver) => Task.Run(() =>
        {
#pragma warning disable CS8602 // Dereference of a possibly null reference.
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
            int selectedId = ((MatchInfoModel)SelectedItem).GetMatchId();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
#pragma warning restore CS8602 // Dereference of a possibly null reference.
            var cts = new CancellationTokenSource();
            

            var stream = SlugChessService.Client.Call.JoinGame(new ChessCom.JoinGameRequest
            {
                Id = selectedId,
                UserIdent = SlugChessService.UserIdent,
                Observer = beObserver
            }, null, null, cts.Token);


            _lobbyState.OnNext((SlugChessService.GetStreamListener(stream, cts.Token), cts));

            return;
            
        });

        private void HandleActivation() 
        {
            RefreshGamesList.Execute().Subscribe();
        }

        private void HandleDeactivation() 
        {
            MatchInfoModels.Clear();
            SelectedItem = null;
        }

        //private void dgMatchesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var dataGrid = sender as DataGrid;
        //    if (dataGrid.SelectedIndex == -1)
        //    {
        //        btJoinGame.IsEnabled = false;
        //    }
        //    else
        //    {
        //        btJoinGame.IsEnabled = true;
        //    }
        //}
    }
}
