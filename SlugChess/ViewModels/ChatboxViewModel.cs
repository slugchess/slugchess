﻿using ChessCom;
using ReactiveUI;
using SlugChess.Services;
using SlugChess.Models;
using System;
using System.Reactive;
using System.Reactive.Disposables;

namespace SlugChess.ViewModels
{
    public class ChatboxViewModel : ViewModelBase, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; } = new ViewModelActivator();

        public ReactiveCommand<Unit, Unit> SendTextCommand { get; }

        //public UserData? OpponentUserData { get; set; } = null;
        public MatchModel? MatchModel { get; set; } = null;

        public string ChatroomName
        {
            get => _chatroomName;
            set => this.RaiseAndSetIfChanged(ref _chatroomName, value);
        }
        private string _chatroomName = "";

        public string MessageText
        {
            get => _messageText;
            set => this.RaiseAndSetIfChanged(ref _messageText, value);
        }
        private string _messageText = "";
        public string RecivedMessagesText
        {
            get => _recivedMessagesText;
            set => this.RaiseAndSetIfChanged(ref _recivedMessagesText, value);
        }
        private string _recivedMessagesText = "";
        public int CaretIndex
        {
            get => _caretIndex;
            set => this.RaiseAndSetIfChanged(ref _caretIndex, value);
        }
        private int _caretIndex = 0;

        public ChatboxViewModel()
        {
            ChatroomName = "CurrentMatch";
            SendTextCommand = ReactiveCommand.Create(() =>  
            {
                if(MessageText.Length > 4 && MessageText.Substring(0, 5) == "/help")
                {
                    SlugChessService.Client.MessageToLocal("'/help' is not implemented yet", "system");
                    MessageText = "";
                    return;
                }
                SlugChessService.Client.Call.SendChatMessage(new ChessCom.ChatMessage
                {
                    Message = MessageText,
                    SenderUsername = SlugChessService.Client.UserData.Username,
                    UserIdent = SlugChessService.UserIdent,
                    ReciverUsertoken = MatchModel?.WhitePlayer.Usertoken ?? "system"
                });
                SlugChessService.Client.Call.SendChatMessage(new ChessCom.ChatMessage
                {
                    Message = MessageText,
                    SenderUsername = SlugChessService.Client.UserData.Username,
                    UserIdent = SlugChessService.UserIdent,
                    ReciverUsertoken = MatchModel?.BlackPlayer.Usertoken ?? "system"
                });
                foreach (var observer in MatchModel.Observers)
                {
                    SlugChessService.Client.Call.SendChatMessage(new ChessCom.ChatMessage
                    {
                        Message = MessageText,
                        SenderUsername = SlugChessService.Client.UserData.Username,
                        UserIdent = SlugChessService.UserIdent,
                        ReciverUsertoken = observer.Usertoken ?? "system"
                    });
                }
                MessageText = "";
            }, this.WhenAnyValue((x)=> x.MessageText, (messageText) => !string.IsNullOrWhiteSpace(messageText)));
            this.WhenActivated(disposables =>
            {

                RecivedMessagesText = RecivedMessagesText + "\n" + "Chat only works when in active game"; 
                CaretIndex = RecivedMessagesText.Length;
                
                SlugChessService.Client.Messages.Subscribe(x => { 
                    RecivedMessagesText = RecivedMessagesText + "\n" + x; 
                    CaretIndex = RecivedMessagesText.Length;
                }).DisposeWith(disposables);
                
                Disposable.Create(() =>
                {
                    RecivedMessagesText = ""; //Clear text as next subscribe to messages will replay all
                }).DisposeWith(disposables);
            });
        }
    }
}
