﻿using ReactiveUI;
using SlugChess.Models;
using Splat;
using ChessCom;
using SlugChess.Services;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;
using System.Windows.Input;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using Google.Protobuf.WellKnownTypes;
using Avalonia.Threading;
using Avalonia.Controls;
using DynamicData.Binding;
using DialogHostAvalonia;
using Serilog;

namespace SlugChess.ViewModels
{
    [DataContract]
    #pragma warning disable 8612
    public class PlayViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
    #pragma warning restore 8612
    {
        public ViewModelActivator Activator { get; }
        public string UrlPathSegment => "/play";
        public IScreen HostScreen { get; }


        public ChessboardViewModel ChessboardVM { get; private set; }
        public ChessClockViewModel ChessClockVM
        {
            get => _chessClockVM;
            set => this.RaiseAndSetIfChanged(ref _chessClockVM, value);
        }
        private ChessClockViewModel _chessClockVM;

        public CapturedPiecesViewModel CapturedPiecesVM
        {
            get => _capturedPiecesVM;
            set => this.RaiseAndSetIfChanged(ref _capturedPiecesVM, value);
        }
        private CapturedPiecesViewModel _capturedPiecesVM;

        public ChatboxViewModel ChatboxVM
        {
            get => _chatboxVM;
            set => this.RaiseAndSetIfChanged(ref _chatboxVM, value);
        }
        private ChatboxViewModel _chatboxVM;

        //public List<ChessboardModel> ChessboardPositions { get; } = new List<ChessboardModel>();

        public string LastMove
        {
            get => _lastMove;
            set => this.RaiseAndSetIfChanged(ref _lastMove, value);
        }
        private string _lastMove = "...";

        public bool WaitingOnMoveReply
        {
            get => _waitingOnMoveReply;
            set => this.RaiseAndSetIfChanged(ref _waitingOnMoveReply, value);
        }
        private bool _waitingOnMoveReply = false;

        public int MoveDisplayIndex
        {
            get => _moveDisplayIndex;
            set => this.RaiseAndSetIfChanged(ref _moveDisplayIndex, value);
        }
        private int _moveDisplayIndex = -1;

        public string PlayingAs => (MatchModel.ThisPlayer.Take(1).Wait()) switch 
            { PlayerIs.White => "Playing as White", PlayerIs.Black => "Playing as Black", PlayerIs.Both => "Playing yourself", PlayerIs.Observer => "Watching as Observer", _ => "No game active" };

        public string PingText
        {
            get => _pingText;
            set => this.RaiseAndSetIfChanged(ref _pingText, value);
        }
        private string _pingText = "";

        public SlugChessService SlugChess => SlugChessService.Client;

        public MatchModel MatchModel { get; }
        private string _matchToken { get; set; } = "0000";

        public IObservable<bool> AwailableVisionDefault { get; }
        public IObservable<bool> AwailableVisionWhite{ get; }
        public IObservable<bool> AwailableVisionBlack{ get; }

        // public ICommand MoveToCreateGame => _moveToCreateGame;
        // private readonly ReactiveCommand<Unit, Unit> _moveToCreateGame;

        // public ICommand MoveToGameBrowser => _moveToGameBrowser;
        // private readonly ReactiveCommand<Unit, Unit> _moveToGameBrowser;

        // public ICommand ViewPgnReplay => _viewPgnReplay;
        // private readonly ReactiveCommand<Window, Unit> _viewPgnReplay;
        /// <summary>
        /// Shift the board to display a move int amount of moves forward or backwards
        /// </summary>
        public ICommand ShiftToMove => _shiftToMove;
        private readonly ReactiveCommand<int, Unit> _shiftToMove;

        public ICommand BackEnd => _backEnd;
        private readonly ReactiveCommand<Unit, Unit> _backEnd;

        public ICommand BackOne => _backOne;
        private readonly ReactiveCommand<Unit, Unit> _backOne;

        public ICommand ForwardOne => _forwardOne;
        private readonly ReactiveCommand<Unit, Unit> _forwardOne;

        public ICommand ForwardEnd=> _forwardEnd;
        private readonly ReactiveCommand<Unit, Unit> _forwardEnd;

        public ICommand ChangeChessboardVision => _changeChessboardVision;
        private readonly ReactiveCommand<string, Unit> _changeChessboardVision;

        public ICommand Surrender => _surrender;
        private readonly ReactiveCommand<Unit, Unit> _surrender;

        //public ReactiveCommand<Unit, LookForMatchResult> ConnectToGame { get; }

        public PlayViewModel(MatchModel matchModel, ReplayModel? replayModel = null, IScreen? screen = null)
        {
            HostScreen = screen ?? Locator.Current?.GetService<IScreen>() ?? throw new InvalidProgramException("Locator is null");
            Activator = new ViewModelActivator();
            MatchModel = matchModel;
            _matchToken = MatchModel.MatchToken;
            MatchModel.ChessboardPositions.ObserveCollectionChanges().Where(x => x.EventArgs.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                .Subscribe(x => MoveDisplayIndex = MatchModel.ChessboardPositions.Count - 1);

            ChessboardVM = new ChessboardViewModel{ CbModel = ChessboardModel.FromDefault()};
            AwailableVisionDefault = Observable.CombineLatest(
                ChessboardVM.WhenAnyValue(x => x.ViewType),
                ChessboardVM.WhenAnyValue(y => y.AwailableViewTypes),
                (viewType, vtAvailable) => (viewType, vtAvailable)
            ).Select( t => t.viewType != ChessboardViewModel.ViewTypes.Default && t.vtAvailable.Contains(ChessboardViewModel.ViewTypes.Default));
            AwailableVisionWhite = Observable.CombineLatest(
                ChessboardVM.WhenAnyValue(x => x.ViewType),
                ChessboardVM.WhenAnyValue(y => y.AwailableViewTypes),
                (viewType, vtAvailable) => (viewType, vtAvailable)
            ).Select( t => t.viewType != ChessboardViewModel.ViewTypes.White && t.vtAvailable.Contains(ChessboardViewModel.ViewTypes.White));
            AwailableVisionBlack = Observable.CombineLatest(
                ChessboardVM.WhenAnyValue(x => x.ViewType),
                ChessboardVM.WhenAnyValue(y => y.AwailableViewTypes),
                (viewType, vtAvailable) => (viewType, vtAvailable)
            ).Select( t => t.viewType != ChessboardViewModel.ViewTypes.Black && t.vtAvailable.Contains(ChessboardViewModel.ViewTypes.Black));
            _changeChessboardVision = ReactiveCommand.Create<string>(s =>
                {
                    ChessboardVM.ViewType = s switch { "Default" => ChessboardViewModel.ViewTypes.Default, 
                    "White" => ChessboardViewModel.ViewTypes.White, 
                    "Black" => ChessboardViewModel.ViewTypes.Black,
                    _ => throw new ArgumentException("Chessboard.ViewType not a valid viewtype")};
                }, 
                MatchModel.ThisPlayerColored.Select(x => !x)
            );

            _chessClockVM = new ChessClockViewModel(MatchModel.IsThisPlayersTurn);
            MatchModel.ChessClock.Subscribe(x => ChessClockVM.SetTime(x.whiteTimeLeft, x.blackTimeLeft, x.currentTurnWhite, x.ticking));
            _capturedPiecesVM = new CapturedPiecesViewModel();

            _chatboxVM = new ChatboxViewModel { MatchModel = MatchModel };
            ChessboardVM.MoveFromTo
                .Where(x => MatchModel.IsThisPlayersTurnNow && MatchModel.OngoingGameNow)
                .Subscribe(x => {
                    (string from, string to) = x;
                    SlugChessService.Client.Call.SendMoveAsync(new MovePacket
                    {
                        AskingForDraw = false,
                        CheatMatchevent = MatchEvent.Non,
                        DoingMove = true,
                        MatchToken = _matchToken,
                        UserIdent = SlugChessService.UserIdent,
                        Move = new Move { From = from, To = to, SecSpent = ChessClockVM.GetSecondsSpent(), Timestamp = Timestamp.FromDateTime(DateTime.UtcNow) },
                    });
                    WaitingOnMoveReply = true;
                }
            );


            _shiftToMove = ReactiveCommand.Create<int>(i => 
            {

                int newVal = i switch 
                {
                    int.MaxValue => MatchModel.ChessboardPositions.Count - 1,
                    int.MinValue => 0,
                    _ => i + MoveDisplayIndex
                };
                if (newVal >= MatchModel.ChessboardPositions.Count - 1)
                {
                    MoveDisplayIndex = MatchModel.ChessboardPositions.Count - 1;
                }
                else if(i + MoveDisplayIndex <= 0)
                {
                    MoveDisplayIndex = 0;
                }
                else
                {
                    MoveDisplayIndex = i + MoveDisplayIndex;
                }
            });

            this.WhenAnyValue(x => x.MoveDisplayIndex).Where(i => i >= 0).Subscribe( i => 
            {
                ChessboardVM.CbModel = MatchModel.ChessboardPositions[i];
                CapturedPiecesVM.Items = MatchModel.ChessboardPositions[i].CapturedPieces.OrderByDescending(x => x.PieceType).ToList();
            });
            this.WhenAnyValue(x => x.WaitingOnMoveReply, x => x.MoveDisplayIndex, (b, i) => !b && i == MatchModel.ChessboardPositions.Count - 1)
                .Subscribe(allowedToMove => ChessboardVM.AllowedToMove = allowedToMove);
            var moveIndexNotZero = this.WhenAnyValue(x => x.MoveDisplayIndex, i => i > 0);
            var moveIndexNotMax = this.WhenAnyValue(x => x.MoveDisplayIndex, i => i < MatchModel.ChessboardPositions.Count-1);
            _backEnd = ReactiveCommand.Create(
                () => { _shiftToMove.Execute(int.MinValue).Subscribe(); },
                moveIndexNotZero
                ); ;
            _backOne = ReactiveCommand.Create(
                () => { _shiftToMove.Execute(-1).Subscribe(); },
                moveIndexNotZero
                );
            _forwardOne = ReactiveCommand.Create(
                () => { _shiftToMove.Execute(1).Subscribe(); },
                moveIndexNotMax
                );
            _forwardEnd = ReactiveCommand.Create(
                () => { _shiftToMove.Execute(int.MaxValue).Subscribe(); },
                moveIndexNotMax
                );
            //Draw and Surrender
            //AskForDraw = MatchModel.AskForDraw;
            //AcceptDraw = MatchModel.AcceptDraw;
            //Surrender = MatchModel.Surrender;
            _surrender = ReactiveCommand.Create(
                () => {
                    DialogHost.GetDialogSession("SurrenderDialogHost")?.Close(false);
                    MatchModel.Surrender.Execute(new Unit());
                }, MatchModel.SurrenderCanExecute
            );

            if(replayModel != null){
                StartUpReplay(replayModel);
            }else{
                StartUpOther(MatchModel.MoveResults);
            }

            this.WhenActivated(disposables =>
            {
                var disp = MatchModel.OngoingGame.Subscribe(x => ((MainWindowViewModel)HostScreen).SetMoveBackEnabled(!x));
                disposables.Add(disp);
                Disposable.Create(() =>
                {

                }).DisposeWith(disposables);
            });
        }

        private void StartUpReplay(ReplayModel replayModel)
        {
            replayModel.MoveResult.Subscribe(
                (moveResult) => Dispatcher.UIThread.InvokeAsync(() =>
                {
                    if (moveResult.GameState != null)
                    {
                        WaitingOnMoveReply = false;
                    }
                })
            );
            replayModel.ExecuteReplayAsync();    
        }

        private void StartUpOther(IObservable<MoveResult> obsevableMoveResults)
        {
            var disposablesForEndMatchWhenViewDeactivated = MatchModel.EndMatchDisposable;
            StartPingService();
            _chessClockVM.ResetTime(MatchModel.WhiteStartTime, MatchModel.BlackStartTime, MatchModel.TimePerMove.Seconds);
            // _chessClockVM.TimeRanOut.Subscribe((x) =>
            // {
            //     if (x) // this hack should not be nessesary. Server need to detect time ran out itself
            //     {
            //         _chessClockVM.StopTimer();
            //         var firstMove = ChessboardVM.CbModel.Moves.First((x) => true);
            //         (string from, string to) = (firstMove.Key, firstMove.Value[0]);
            //         SlugChessService.Client.Call.SendMoveAsync(new MovePacket
            //         {
            //             AskingForDraw = false,
            //             CheatMatchevent = MatchEvent.Non,
            //             DoingMove = true,
            //             MatchToken = MatchModel.MatchToken,
            //             UserIdent = SlugChessService.UserIdent,
            //             Move = new Move { From = from, To = to, SecSpent = ChessClockVM.GetSecondsSpent(), Timestamp = Timestamp.FromDateTime(DateTime.UtcNow) },
            //         });
            //         WaitingOnMoveReply = true;
            //     }
            // }).DisposeWith(disposablesForEndMatchWhenViewDeactivated);
            Activator.Deactivated.Subscribe((u) =>
            {
                SlugChessService.Client.Call.SendMoveAsync(new MovePacket
                {
                    CheatMatchevent = MatchEvent.ExpectedClosing,
                    DoingMove = false,
                    MatchToken = MatchModel.MatchToken,
                    UserIdent = SlugChessService.UserIdent,
                });
            }).DisposeWith(disposablesForEndMatchWhenViewDeactivated);
            
            obsevableMoveResults.Subscribe(
                (moveResult) => Dispatcher.UIThread.InvokeAsync(() =>
                {
                    if (moveResult.GameState != null)
                    {

                        WaitingOnMoveReply = false;
                    }
                    if (moveResult.GameResult != null)
                    {
                        SaveMatchPgn(moveResult.GameResult.Pgn, MatchModel.WhitePlayer.Username, MatchModel.BlackPlayer.Username);
                    }
                }), 
                (error) => Dispatcher.UIThread.InvokeAsync(() =>
                {
                    MainWindowViewModel.SendNotification("Connection error");
                    SlugChessService.Client.MessageToLocal("Connection error:  " + error.ToString(), "system");
                    ChessClockVM.StopTimer();
                    disposablesForEndMatchWhenViewDeactivated.Dispose();
                }), () => Dispatcher.UIThread.InvokeAsync(() =>
                {
                    ChessClockVM.StopTimer();
                    disposablesForEndMatchWhenViewDeactivated.Dispose();
                    SlugChessService.Client.GetNewUserdata();
                })
            );
        }

        private void StartPingService(){
            var cancellationTokenSource = new CancellationTokenSource();
            MatchModel.EndMatchDisposable.Add(cancellationTokenSource);
            var pingServiceStream = SlugChessService.Client.Call.PingService();
            pingServiceStream.RequestStream.WriteAsync(new PingResponse{UserIdent = SlugChessService.UserIdent});
            Task.Run(() => {
                try {
                    var response = new PingResponse{Ping = new PingResponse.Types.Ping{Timestamp = Timestamp.FromDateTime(DateTime.UtcNow), Token = 0}};
                    while ( !cancellationTokenSource.Token.IsCancellationRequested && 
                            pingServiceStream.ResponseStream.MoveNext(cancellationTokenSource.Token).Result){
                        //Thread.Sleep(123);
                        response.Ping.Token = pingServiceStream.ResponseStream.Current.Token;
                        response.Ping.Timestamp = Timestamp.FromDateTime(DateTime.UtcNow);
                        //Thread.Sleep(123);
                        var sendt = pingServiceStream.RequestStream.WriteAsync(response);
                        var ping = pingServiceStream.ResponseStream.Current.CurrentPing;
                        var offset = pingServiceStream.ResponseStream.Current.CurrentClockOffset;
                        //Dispatcher.UIThread.InvokeAsync(() => {
                        //    PingText = $"Ping {ping}ms";
                        //});
                        PingText= $"Ping {ping}ms";
                        sendt.Wait();
                    }
                    PingText = $"";
                } catch(OperationCanceledException ex){
                    if(ex.CancellationToken.IsCancellationRequested){
                        Log.Debug("OperationCanceledException handled");
                    }else {
                        throw;
                    }
                } catch(ObjectDisposedException ex){
                    Log.Debug(ex.StackTrace);
                }
            });
        }


        /// <summary>
        /// Allready determined result.Success is true
        /// </summary>
        /// <param name="result"></param>
        public static MatchModel BootUpMatch(MatchResult matchResult)
        {
            PlayerIs playerIs = PlayerIs.Non;
            if(SlugChessService.Usertoken == matchResult.WhiteUserData.Usertoken){
                //MainWindowViewModel.SendNotification("You are playing agains " + matchResult.BlackUserData.Username + " as white");
                //Chatbox.OpponentUsertoken = matchResult.BlackUserData.Usertoken;
                playerIs = PlayerIs.White;

            }else if(SlugChessService.Usertoken == matchResult.BlackUserData.Usertoken){
                //MainWindowViewModel.SendNotification("You are playing agains " + matchResult.WhiteUserData.Username + " as black");
                //Chatbox.OpponentUsertoken = matchResult.WhiteUserData.Usertoken;
                playerIs = PlayerIs.Black;

            }else if(matchResult.Observers.Any(x => x.Usertoken == SlugChessService.Usertoken)){
                playerIs = PlayerIs.Observer;
            }else{
                throw new ArgumentException("player must be one of these");
            }
            var playerTime = new TimeSpan(0, matchResult.GameRules.TimeRules.PlayerTime.Minutes, matchResult.GameRules.TimeRules.PlayerTime.Seconds);
            ShellHelper.PlaySoundFile(Program.RootDir + "Assets/sounds/match_start.wav");
            var matchObservable = SlugChessService.Client.GetMatchListener(matchResult.MatchToken);
           

            return new MatchModel(matchResult.MatchToken, matchObservable, playerIs, matchResult.WhiteUserData, matchResult.BlackUserData, matchResult.Observers.ToList(), playerTime, playerTime, new TimeSpan(0, 0, matchResult.GameRules.TimeRules.SecondsPerMove));
            
        }
        public static IObservable<Replay?> ViewPgnReplayImpl(Window window) => Observable.Start(() =>
        {
            OpenFileDialog dialog = new OpenFileDialog { AllowMultiple = false };
            dialog.Directory = Program.Settings.ShareDir + "games_database";
            //dialog.Directory = "D:\\Projects";
            dialog.Title = "Select 'pgn' file to watch replay";
            dialog.Filters?.Add(new FileDialogFilter() {Name="chess notation", Extensions = { "pgn" },  });

            string[] result = dialog.ShowAsync(window)?.Result ?? new string[0];
            if (result?.Length == 1 )
            {
                return Dispatcher.UIThread.InvokeAsync<Replay?>(() =>
                {
                    MainWindowViewModel.SendNotification($"PGN filepath '{result[0]}'");
                    var replay =  SlugChessService.Client.Call.ProcessReplay(new ReplayRequest { Pgn = File.ReadAllText(result[0]), UserIdent=SlugChessService.UserIdent});
                    return replay.Valid?replay:null;

                }).Result;
            }
            return null;
        });

        private void SaveMatchPgn(string pgn, string whiteUsername, string blackUsername)
        {
            if (!Directory.Exists(Program.Settings.ShareDir + "games_database")) Directory.CreateDirectory(Program.Settings.ShareDir + "games_database");
            if (!Directory.Exists(Program.Settings.ShareDir + "games_database/last_few")) Directory.CreateDirectory(Program.Settings.ShareDir + "games_database/last_few");
            File.WriteAllText(Program.Settings.ShareDir + "games_database/latest.pgn", pgn);
             if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                File.WriteAllText(Program.Settings.ShareDir + $"games_database/last_few/{DateTime.UtcNow:yyyy-MM-dd-HH-mm-ss}_{Program.GetSafeFilename(whiteUsername)}_{Program.GetSafeFilename(blackUsername)}.pgn", pgn);
             }else{
                 File.WriteAllText(Program.Settings.ShareDir + $"games_database/last_few/{DateTime.UtcNow:yyyy-MM-dd-HH-mm-ss}_{whiteUsername}_{blackUsername}.pgn", pgn);
             }
        }
    }
}
