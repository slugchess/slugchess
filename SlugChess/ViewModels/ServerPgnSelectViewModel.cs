using System;
using System.Windows.Input;
using System.Threading;
using System.Threading.Tasks;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Linq;
using System.Collections.Generic;
using Splat;
using ReactiveUI;
using Serilog;
using Grpc.Core;
using ChessCom;
using SlugChess.Services;

namespace SlugChess.ViewModels
{
    public class ServerPgnSelectViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
    {

        public class PgnData {
            public DateTime DatePlayed { get; set; }
            public string? WhitePlayer { get; set; }
            public string? BlackPlayer { get; set; }
            public string Event {get; set;} = "{Not Implemented Yet}";

        }
        public string UrlPathSegment => "/serverpgnselect";
        public IScreen HostScreen { get; }
        public ViewModelActivator Activator { get; }

        public SearchResult SearchResult
        {
            get => _searchResult;
            set => this.RaiseAndSetIfChanged(ref _searchResult, value);
        }
        private SearchResult _searchResult = new SearchResult();

        public List<PgnData> PgnList
        {
            get => _pgnList;
            set => this.RaiseAndSetIfChanged(ref _pgnList, value);
        }
        private List<PgnData> _pgnList = new List<PgnData>();

        public int GameIndex
        { 
            get => _gameIndex;
            set => this.RaiseAndSetIfChanged(ref _gameIndex, value);
        }
        private int _gameIndex = -1;


        public string UsernameFilter
        { 
            get => _usernameFilter;
            set => this.RaiseAndSetIfChanged(ref _usernameFilter, value);
        }
        private string _usernameFilter = "";

        private ReactiveCommand<GamesDatabaseQuery, GamesDatabaseResult> _queryGames { get; }


        private ReactiveCommand<Unit, Replay> _viewReplayCommand { get; }
        public ICommand ViewReplayCommand => _viewReplayCommand;
        public IObservable<Replay> ViewReplay => _viewReplayCommand;

        private ReactiveCommand<Unit, Unit> _refreshSearch { get; }
        public ICommand RefreshSearch => _refreshSearch;

        private Subject<Replay> _replaySelected = new Subject<Replay>();
        public IObservable<Replay> ReplaySelected => _replaySelected;

        public ServerPgnSelectViewModel(IScreen? screen = null)
        {
            HostScreen = screen ?? Locator.Current?.GetService<IScreen>() ?? throw new InvalidProgramException("Locator is null");
            Activator = new ViewModelActivator();

            _viewReplayCommand = ReactiveCommand.Create(()=>{
                var gameResult = SlugChessService.Client.Call.RequestPgnGame(new PgnGameRequest{
                    Game=SearchResult.Games[GameIndex].Filename,
                    UserIdent=SlugChessService.UserIdent
                });
                
                return SlugChessService.Client.Call.ProcessReplay(new ReplayRequest{Pgn=gameResult.Pgn, UserIdent=SlugChessService.UserIdent});
            }, this.WhenAnyValue((x) => x.GameIndex, (int x) => x >= 0) );
            _viewReplayCommand.Subscribe((x)=> {
                _replaySelected.OnNext(x);
            });

            _queryGames = ReactiveCommand.CreateFromTask<GamesDatabaseQuery, GamesDatabaseResult>((GamesDatabaseQuery query) => 
                {
                    return Task.Run<GamesDatabaseResult>(() => {
                        try {
                            return SlugChessService.Client.Call.QueryGamesDatabase(query);
                        }
                        catch (RpcException ex)
                        {
                            Log.Error($"RpcException in GamesDatabaseQuery. Status: {ex.Status}. Message: {ex.Message}");
                            return new GamesDatabaseResult();
                        }
                        catch(InvalidOperationException ex){
                            Log.Error($"Exception in GamesDatabaseQuery. Message: {ex.Message}");
                            return new GamesDatabaseResult();
                        }
                    });
                }
            );
            
            _queryGames.Subscribe(x => {
                if(x.ResultCase == GamesDatabaseResult.ResultOneofCase.Search){
                    SearchResult = x.Search;
                    PgnList = x.Search.Games.Select((value) => new PgnData{DatePlayed = value.Timestamp.ToDateTime(),WhitePlayer=value.WhiteUsername,BlackPlayer=value.BlackUsername}).ToList();
                } else {
                    MainWindowViewModel.SendNotification("Games search error: " + x.FailedMessage);
                    Log.Error("Games Database search failed: " + x.FailedMessage);
                }

            });
            
            _refreshSearch = ReactiveCommand.Create(() => 
                {
                    _queryGames.Execute(new GamesDatabaseQuery{
                        GameSearch=new GameSearch{SearchFilter=new SearchFilter{Username=UsernameFilter}},
                        UserIdent=SlugChessService.UserIdent}).Take(1).Subscribe();
                }
            , _queryGames.IsExecuting.Select( x => !x));

            this.WhenActivated(disposables =>
            {
                // Use WhenActivated to execute logic
                // when the view model gets activated.
                this.HandleActivation();

                // Or use WhenActivated to execute logic
                // when the view model gets deactivated.
                Disposable.Create(() => this.HandleDeactivation()).DisposeWith(disposables);
            });
        }


        private void HandleActivation() 
        {
            UsernameFilter = SlugChessService.Client.UserData.Username;

            _refreshSearch.Execute().Subscribe();
        }

        private void HandleDeactivation() 
        {

        }

    }
}