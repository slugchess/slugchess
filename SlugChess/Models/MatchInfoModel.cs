﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessCom;

namespace SlugChess.Models
{
    public enum PlayerIs
    {
        Non = 0,
        White = 1,
        Black = 2,
        Both = 3,
        Observer = 4
    }

    public class MatchInfoModel
    {

        public string Host { get; set; }
        public int HostElo { get; set; }
        public string Variant { get; set; }
        //public string ChessType { get; set; }
        public string SideType { get; set; }
        public string Time { get; set; }
        //public string VisionRules { get; set; }
        public bool CanObserve { get; set;}
        private int _matchId;
        public int GetMatchId() => _matchId;
        private string _hostUsertoken;

        private MatchInfoModel(string host, int hostElo, string variant, string sideType, string time, bool canObserve, int matchId, string hostUsertoken)
        {
            Host = host;
            HostElo = hostElo;
            Variant = variant;
            //ChessType = chessType;
            SideType = sideType;
            Time = time;
            CanObserve = canObserve;
            //VisionRules = visionRules;
            _matchId = matchId;
            _hostUsertoken = hostUsertoken;
        }

        public string GetHostUsertoken() => _hostUsertoken;

        public static List<MatchInfoModel> FromChesscom(HostedGamesMap hostedGamesMap)
        {
            var matches = new List<MatchInfoModel>();
            foreach (var keyVal in hostedGamesMap.HostedGames)
            {
                var gameRules = keyVal.Value.GameRules;
                var match = new MatchInfoModel(
                    keyVal.Value.Host.Username,
                    (int)Math.Round(keyVal.Value.Host.Elo),
                    (keyVal.Value.GameRules.VariantCase == GameRules.VariantOneofCase.Named? keyVal.Value.GameRules.Named:"Custom"),
                    //keyVal.Value.GameRules.ChessType.ToString(),
                    keyVal.Value.GameRules.SideType.ToString(),
                    TimeRulesToString(keyVal.Value.GameRules.TimeRules),
                    keyVal.Value.ObserversAllowed,
                    //gameRules.VisionRulesCase == GameRules.VisionRulesOneofCase.TypeRules?"SlugChess."+ gameRules.TypeRules:throw new NotImplementedException("aaaaaaaaaahahahaaaaaahaaaa"),
                    keyVal.Value.Id,
                    keyVal.Value.Host.Usertoken
                    );
                matches.Add(match);
            }

            return matches;
        }

        public static List<MatchInfoModel> FromTestData()
        {
            return new List<MatchInfoModel> { new MatchInfoModel("frank", 9999, "Tourch", ChessCom.SideType.Random.ToString(), "gucvk all", true, 153135, "tickenthing")
                , new MatchInfoModel("dannnr", 8888, "Sigith", ChessCom.SideType.Random.ToString(), "gucvk all", false, 153136, "tickenthing") };
        }

        public static string TimeRulesToString(TimeRules timeRules)
        {
            if (timeRules == null) return "<null>";
            return $"{timeRules.PlayerTime.Minutes}min + {timeRules.SecondsPerMove}s";
        }

    }
}
