using System;
using ChessCom;
using Grpc.Core;
using System.Collections.Generic;
using System.Text;
using Avalonia.Threading;
using System.Reactive.Subjects;
using SlugChess.Services;
using SlugChess.ViewModels;
using System.Reactive;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Windows.Input;
using System.Threading;
using Google.Protobuf;
using System.IO;
using System.Collections.ObjectModel;
using Avalonia.Collections;
using DynamicData.Binding;
using System.Linq;
using System.Threading.Tasks;

namespace SlugChess.Models
{
    public class ReplayModel {

        


        public IObservable<MoveResult> MoveResult => _moveResultSubject; 
        private Subject<MoveResult> _moveResultSubject = new Subject<MoveResult>();
        public Replay Replay => _replay;
        private Replay _replay;

        public ReplayModel(Replay replay)
        {
            _replay = replay;
        }

        public void ExecuteReplayAsync(){
            Task.Run(() => {
                for (int i = 0; i < _replay.GameStates.Count; i++)
                {
                    Thread.Sleep(100);
                    _moveResultSubject.OnNext(new MoveResult
                    {
                        GameState = _replay.GameStates[i],
                        MatchEvent = (i + 1 != _replay.GameStates.Count ? MatchEvent.Non : _replay.MatchEvent),
                        ChessClock = new ChessClock { BlackSecondsLeft = 0, WhiteSecondsLeft = 0, TimerTicking = false }
                    });

                }
                _moveResultSubject.OnCompleted();
            });
        }
    }
}