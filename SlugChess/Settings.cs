
using System;
using System.IO;
using System.Text.Json.Serialization;
using Serilog;

namespace SlugChess
{
    public class Settings
    {

#if DEBUG
        const string SlugChessDirName = "SlugChess-debug";     
#else
        const string SlugChessDirName = "SlugChess";     
#endif

        [JsonIgnore]
        public string ConfigDir { get {
            if(Program.InitSettings.CustomConfigDir=="") 
                return System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create) + $"/{SlugChessDirName}/"; 
            else 
                return Program.InitSettings.CustomConfigDir; } 
        }
        [JsonIgnore]
        public string ShareDir { get {
            if(Program.InitSettings.CustomShareDir=="") 
                return System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData, Environment.SpecialFolderOption.Create) + $"/{SlugChessDirName}/"; 
            else 
                return Program.InitSettings.CustomShareDir; } 
        }

        public bool PostLoadCleanup(){
            try{
                if(!Directory.Exists(ConfigDir)){
                    Directory.CreateDirectory(ConfigDir);
                }
                if(!Directory.Exists(ShareDir)){
                    Directory.CreateDirectory(ShareDir);
                }
                return true;
            }
            catch(Exception ex){
                Log.Warning("Post load cleanup exception: " + ex.Message);
                return false;
            }
        }
    }


    public class InitSettings
    {
        public string CustomConfigDir { get; set; } = "";
        public string CustomShareDir { get; set; } = "";

        public bool PostLoadCleanup(){
            try{
                if(CustomConfigDir.StartsWith('.')) CustomConfigDir = Program.RootDir+CustomConfigDir;
                if(CustomShareDir.StartsWith('.')) CustomShareDir = Program.RootDir+CustomShareDir;
                if(!(CustomConfigDir.EndsWith('/') || CustomConfigDir.EndsWith('\\'))) CustomConfigDir = CustomConfigDir+"/";
                if(!(CustomShareDir.EndsWith('/') || CustomShareDir.EndsWith('\\'))) CustomShareDir = CustomShareDir+"/";
                if(!Directory.Exists(CustomConfigDir)) Directory.CreateDirectory(CustomConfigDir);
                if(!Directory.Exists(CustomShareDir)) Directory.CreateDirectory(CustomShareDir);
                return true;
            }
            catch(Exception ex){
                Log.Warning("Init post load cleanup exception: " + ex.Message);
                return false;
            }
        }
    }
}
