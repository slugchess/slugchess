﻿using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using SlugChess.ViewModels;
using SlugChess.Views;
using SlugChess.Services;
using ReactiveUI;
using Splat;
using Avalonia.ReactiveUI;
using SlugChess.Drivers;
using System.IO;
using System.Reflection;

namespace SlugChess
{
    public class App : Application
    {

        public static void Shutdown(){
            _lifetime?.Shutdown();
        }

        private static IClassicDesktopStyleApplicationLifetime? _lifetime;
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            AssetBank.LoadAssets();
            _lifetime = (IClassicDesktopStyleApplicationLifetime?)ApplicationLifetime;
            // Initialize suspension hooks.
            var suspension = new AutoSuspendHelper(ApplicationLifetime?? new ClassicDesktopStyleApplicationLifetime());
            RxApp.SuspensionHost.CreateNewAppState = () => new MainWindowViewModel();
            //RxApp.SuspensionHost.SetupDefaultSuspendResume(new NewtonsoftJsonSuspensionDriver("appstate.json"));
            RxApp.SuspensionHost.SetupDefaultSuspendResume(new NewtonsoftJsonSuspensionDriver(Program.Settings.ConfigDir + "/appstate.json"));
            suspension.OnFrameworkInitializationCompleted();

            // Read main view model state from disk.
            var state = RxApp.SuspensionHost.GetAppState<MainWindowViewModel>();
            Locator.CurrentMutable.RegisterConstant<IScreen>(state);
            // Register views.
            Locator.CurrentMutable.Register<IViewFor<ChessboardViewModel>>(() => new Chessboard());
            Locator.CurrentMutable.Register<IViewFor<PlayViewModel>>(() => new PlayView());
            Locator.CurrentMutable.Register<IViewFor<LoginViewModel>>(() => new LoginView());
            Locator.CurrentMutable.Register<IViewFor<StartMenuViewModel>>(() => new StartMenuView());
            Locator.CurrentMutable.Register<IViewFor<CreateGameViewModel>>(() => new CreateGameView());
            Locator.CurrentMutable.Register<IViewFor<GameBrowserViewModel>>(() => new GameBrowserView());
            Locator.CurrentMutable.Register<IViewFor<ChessClockViewModel>>(() => new ChessClock());
            Locator.CurrentMutable.Register<IViewFor<CapturedPiecesViewModel>>(() => new CapturedPieces());
            Locator.CurrentMutable.Register<IViewFor<ChatboxViewModel>>(() => new Chatbox());
            Locator.CurrentMutable.Register<IViewFor<RegisterUserViewModel>>(() => new RegisterUserView());
            Locator.CurrentMutable.Register<IViewFor<LobbyViewModel>>(() => new LobbyView());
            Locator.CurrentMutable.Register<IViewFor<ServerPgnSelectViewModel>>(() => new ServerPgnSelectView());

            
            // Show the main window.
            new MainWindow { DataContext = Locator.Current.GetService<IScreen>() }.Show();
            base.OnFrameworkInitializationCompleted();
        }
    }
}
