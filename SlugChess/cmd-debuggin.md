## Running from the console
dotnet run -c Debug -- --debugLogin debugman  --address localhost --port 43326 --no-updatecheck

To add grpc connection debug logging add this envionment variable `GRPC_VERBOSITY=DEBUG`