﻿using SharpCompress.Archives;
using SharpCompress.Archives.SevenZip;
using SharpCompress.Common;
using SharpCompress.Readers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text.Json;
using System.Net.Sockets;

namespace SlugChessUpdater
{
    public static class Const
    {
        public static int FileMajorPart => Assembly.GetEntryAssembly()?.GetName()?.Version?.Major ?? 0;
        public static int FileMinorPart => Assembly.GetEntryAssembly()?.GetName()?.Version?.Minor ?? 0;
        public static int FileBuildPart => Assembly.GetEntryAssembly()?.GetName()?.Version?.Build ?? 0;
        public static string GetVersion()
        {
            return $"{FileMajorPart}.{FileMinorPart}.{FileBuildPart}";
        }

        public static string SlugchessFilename7z(int major, int minor, int patch) => $"slugchess_{major}_{minor}_{patch}_{PlatformString}.7z";
#if DEBUG
        public static string VersionUrl = "https://slugchess.com/download-debug/latest/version.txt";
        public static string SlugChessUrl(int major, int minor, int patch) => $"https://slugchess.com/download-debug/releases/{PlatformString}/{SlugchessFilename7z(major,minor,patch)}";
#endif

        public static string GitlabProjectUrl = "https://gitlab.com/api/v4/projects/38448404";
        public static string ReleasesUrl = $"{GitlabProjectUrl}/releases?sort=desc&order_by=released_at";
        public static string UpdaterMaxVersionUrl = $"{GitlabProjectUrl}/repository/files/updater-max-version.json/raw?ref=main";
        public static string PlatformString 
        { get {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    return "linux-x64";
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    return "win-x64";
                }
                throw new PlatformNotSupportedException("fucj esg");
            } 
        }
    }

    static class Program
    {
        //private static bool _downloadCompleted;
        private static string _downloadName = "non";
        private static int _downloadPercent = 0;

        public static string RootDir = AppContext.BaseDirectory;


        static void Main(string[] args)
        {
            try
            {

                List<string> argsList = args.ToList();
                if (argsList.Contains("--help"))
                {
                    Console.WriteLine(
                        $"SlugChessUpdator help - version {GetVersion()}\n"+
                        "    All commands retun Error:[message] if something is wrong     \n" +
                        "--help                      - for this message                   \n" +
                        "--check-version xx.xx.xx    - to to check version relative       \n" +
                        "    to latest version, returns 0 for ok, 1 for must,             \n" +
                        "    2 for should update, 3 must fetch new version manualy        \n" +
                        "--run-update                - to update SlugChess,               \n" +
                        "    SlugChess has 2 sec to quit after starting update,           \n" +
                        "    SlugChess will start after update complete                   \n" 
                        );
                }
                else if (argsList.Contains("--version"))
                {
                    Console.WriteLine("SlugChessUpdater " + GetVersion());                
                }
                else if(argsList.Contains("--check-version"))
                {
                    List<int> currentVersion = argsList[argsList.IndexOf("--check-version") + 1].Split('.').ToList().ConvertAll(Convert.ToInt32);

                    string newestVersionUpdatable = WebFetcher.GetVersionUpdatable();
                    if(newestVersionUpdatable != ""){
                        List<int> newestVersion = newestVersionUpdatable.Split('.').ToList().ConvertAll(Convert.ToInt32);
                        if (currentVersion[0] == newestVersionUpdatable[0])
                        {
                            if(currentVersion[1] == newestVersionUpdatable[1])
                            {
                                if(currentVersion[2] > newestVersionUpdatable[2])
                                {
                                    Console.WriteLine("3");
                                    return;
                                }
                            }
                            else if (currentVersion[1] > newestVersionUpdatable[1])
                            {
                                Console.WriteLine("3");
                                return;
                            }
                        }
                        else if(currentVersion[0] > newestVersionUpdatable[0])
                        {
                            Console.WriteLine("3");
                            return;
                        }
                    }

                    List<int> latestVersion = WebFetcher.GetLatestVersionString().Split('.').ToList().ConvertAll(Convert.ToInt32);
                    if (latestVersion[0] > currentVersion[0])
                    {
                        Console.WriteLine("1");
                        return;
                    }
                    else if(latestVersion[0] == currentVersion[0])
                    {
                        if (latestVersion[1] > currentVersion[1])
                        {
                            Console.WriteLine("1");
                            return;
                        }
                        else if (latestVersion[1] == currentVersion[1])
                        {
                            if(latestVersion[2] > currentVersion[2])
                            {
                                Console.WriteLine("2");
                                return;
                            }
                            else if (latestVersion[2] == currentVersion[2])
                            {
                                Console.WriteLine("0");
                                return;
                            }
                            else
                            {
                                Console.WriteLine("Error:current version is higher than latest");
                                return;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Error:current version is higher than latest");
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error:current version is higher than latest");
                        return;
                    }
                }
                else if (argsList.Contains("--run-update"))
                {
                    
                    var versionString = "0.0.0";
                    if(argsList.Count > argsList.IndexOf("--run-update") + 1 && argsList[argsList.IndexOf("--run-update") + 1].Split('.').ToList().ConvertAll(Convert.ToInt32).Count() == 3){
                        versionString = argsList[argsList.IndexOf("--run-update") + 1];
                        Console.WriteLine("Update to " + versionString);
                    }else{
                        versionString = WebFetcher.GetLatestVersionString();
                        Console.WriteLine("Update to latest. " + versionString);
                    }
                    //Do update
                    DeleteFiles();
                    
                    DownloadAndExtractNewVersion(WebFetcher.GetVersionAddress(versionString), versionString);
                    Console.WriteLine("Update complete. Launching SlugChess");
                    LaunchAval();
                }
                else if (argsList.Contains("--test"))
                {
                }
            }
            catch(PlatformNotSupportedException)
            {
                Console.WriteLine("Error: Could not detect plattform");
                Console.WriteLine("Press any to quit...");
                Console.ReadKey();
            }
            catch(Exception ex)
            {
                Console.WriteLine("unknown error: " + ex.Message);
                Console.WriteLine("Press any to quit...");
                Console.ReadKey();
            }
        }

        public static string GetVersion()
        {
            return $"{Const.FileMajorPart}.{Const.FileMinorPart}.{Const.FileBuildPart}";
        }

        static void DeleteFiles()
        {
            Console.WriteLine("Deleting old version");
            string[] filePaths = Directory.GetFiles($"{RootDir}");
            foreach (string filePath in filePaths)
            {
                var name = new FileInfo(filePath).Name;
                var nameLower = name.ToLower();
                if (nameLower != "appstate.json" && 
                    nameLower != "settings.json" && 
                    nameLower != "initsettings.json" && 
                    nameLower != "slugchessupdater.exe" && 
                    nameLower != "slugchessupdater")
                {
                    //Console.WriteLine(nameLower);
                    File.Delete(filePath);
                }
            }
            string[] directoryPaths = Directory.GetDirectories($"{RootDir}");
            foreach (string dirPath in directoryPaths)
            {
                var name = new DirectoryInfo(dirPath).Name;
                var nameLower = name.ToLower();
                if (nameLower != "Mods" && 
                    nameLower != "Safe" )
                {
                    Directory.Delete(dirPath, true);
                }
            }


            Console.WriteLine("Deleting old files complete");

        }

        static void DownloadAndExtractNewVersion(string adressOfLatestVersion, string latestVersion)
        {
            Console.WriteLine("Downloading SlugChess " + latestVersion + ": " + adressOfLatestVersion);
            Directory.CreateDirectory($"{RootDir}temp");

            HttpClient client = new HttpClient();
            Uri? uriResult;
            var outputPath = $"{RootDir}temp/latest.7z";

            if (!Uri.TryCreate(adressOfLatestVersion, UriKind.Absolute, out uriResult))
                throw new InvalidOperationException("URI is invalid.");


            byte[] fileBytes = client.GetByteArrayAsync(adressOfLatestVersion).Result;
            File.WriteAllBytes(outputPath, fileBytes);

            Console.WriteLine($"Download of {outputPath} complete");

            // WebClient myWebClient = new WebClient();

            // // Specify that the DownloadFileCallback method gets called
            // // when the download completes.
            // myWebClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCallback);
            // // Specify a progress notification handler.
            // myWebClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);
            // _downloadName = "SlugChess " + latestVersion;
            // _downloadCompleted = false;
            // _downloadPercent = 0;
            // myWebClient.DownloadFileAsync(new Uri(adressOfLatestVersion), $"{RootDir}temp/latest.7z");

            // //Wait until download completed
            // while (!_downloadCompleted) Thread.Sleep(250);
            Console.WriteLine("Extracting SlugChess " + latestVersion);
            Console.WriteLine("Size of achive: " + new FileInfo($"{RootDir}temp/latest.7z").Length);
            using (var archive = SevenZipArchive.Open($"{RootDir}temp/latest.7z"))
            {
                var reader = archive.ExtractAllEntries();
                reader.CompressedBytesRead += new EventHandler<CompressedBytesReadEventArgs>(CompressedBytesReadCallback);
                while (reader.MoveToNextEntry())
                {
                    if (!reader.Entry.IsDirectory)
                    {
                        reader.WriteEntryToDirectory($"{RootDir}temp", new ExtractionOptions() { ExtractFullPath = true, Overwrite = true });
                    }
                }
            }
            Console.WriteLine("Moving files");
            foreach (var file in new DirectoryInfo($"{RootDir}temp/SlugChess/").GetFiles())
            {
                if (File.Exists($"{RootDir}{file.Name}"))
                {
                    Console.WriteLine("File allready exists: " + file.Name);
                    if(file.Name == "SlugChessUpdater.exe" || file.Name == "SlugChessUpdater")
                    {
                        file.MoveTo($"{RootDir}{file.Name}.new");
                    }
                }
                else
                {
                    file.MoveTo($"{RootDir}{file.Name}");
                }
            }
            foreach (var dir in new DirectoryInfo($"{RootDir}temp/SlugChess/").GetDirectories())
            {
                dir.MoveTo($"{RootDir}{dir.Name}");
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)){
                Bash($"chmod -v -R u+rwX,go+rwX {RootDir} && "+
                $"chmod -v a+x {RootDir}SlugChess && " +
                $"chmod -v a+x {RootDir}SlugChessUpdater.new");
            }
            Console.WriteLine("Deleting temp files");
            int attempts = 0;
            while(attempts < 3)
            {
                attempts++;
                try
                {
                    Directory.Delete($"{RootDir}temp", true);
                    break;
                }
                catch (UnauthorizedAccessException)
                {
                    Console.WriteLine("Deleting temp failed. Retrying...");
                    Thread.Sleep(500);
                }
            }
            
        }

        static void LaunchAval()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                Process.Start("SlugChess.exe", "--no-updatecheck");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Process.Start("SlugChess", "--no-updatecheck");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                //bla bla bla
                throw new PlatformNotSupportedException();
            }
            else
            {
                throw new PlatformNotSupportedException();
            }
        }

        private static void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            // Displays the operation identifier, and the transfer progress.
            if(_downloadPercent < e.ProgressPercentage)
            {
                _downloadPercent = e.ProgressPercentage;
                Console.WriteLine("{0}    downloaded {1} of {2} bytes. {3} % complete...",
                    _downloadName,
                    e.BytesReceived,
                    e.TotalBytesToReceive,
                    e.ProgressPercentage);
            }

        }

        private static void DownloadFileCallback(object? sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("Download failed. Error: " + e.Error?.Message);
            }
            else
            {
                Console.WriteLine(_downloadName + " download finished");
                //_downloadCompleted = true;
            }
        }

        private static void CompressedBytesReadCallback(object? sender, CompressedBytesReadEventArgs e)
        {
            Console.WriteLine("Compressed bytes read: " + e.CompressedBytesRead);
        }

        private static string Bash(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");
                
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            return result;
        }



    }


    public static class WebFetcher
    {
        public class IncompleteLinks 
        {
            public string name {get; set; } = "";
            public string url {get; set; } = "";
            public bool external { get; set; }
            public string link_type {get; set; } = "";
        }
        public class IncompleteAssets
        {
            public IList<IncompleteLinks> links { get; set; } = new List<IncompleteLinks>();
        }
        public class IncompleteRelease
        {
            public string tag_name { get; set; } = "";
            public bool upcoming_release { get; set; }
            public IncompleteAssets assets { get; set; } = new IncompleteAssets();
        }

        private static HttpClient _client = new HttpClient(new SocketsHttpHandler() {
        ConnectCallback = async (context, cancellationToken) => {
            // Use DNS to look up the IP address(es) of the target host
            IPHostEntry ipHostEntry = await Dns.GetHostEntryAsync(context.DnsEndPoint.Host);

            // Filter for IPv4 addresses only
            IPAddress ipAddress = ipHostEntry.AddressList.FirstOrDefault(i => i.AddressFamily == AddressFamily.InterNetwork);

            // Fail the connection if there aren't any IPV4 addresses
            if (ipAddress == null) {
                throw new Exception($"No IP4 address for {context.DnsEndPoint.Host}");
            }

            // Open the connection to the target host/port
            TcpClient tcp = new();
            await tcp.ConnectAsync(ipAddress, context.DnsEndPoint.Port, cancellationToken);

            // Return the NetworkStream to the caller
            return tcp.GetStream();
        }
        })
        {
            DefaultRequestVersion =  HttpVersion.Version11,
            DefaultVersionPolicy = HttpVersionPolicy.RequestVersionExact
        };
        public static string GetVersionUpdatable()
        {
            //Console.WriteLine($"Fetching {Const.UpdaterMaxVersionUrl}\n");
            using HttpResponseMessage response = _client.GetAsync(Const.UpdaterMaxVersionUrl).Result;
            using HttpContent content = response.Content;
            Dictionary<string,string> versions = JsonSerializer.Deserialize<Dictionary<string,string>>(content.ReadAsStringAsync().Result)!;
            if(versions.ContainsKey(Const.GetVersion())){
                return versions[Const.GetVersion()];
            }else {
                return "0.0.0";
            }
        }
        public static string GetLatestVersionString()
        {
#if DEBUG
            
            using HttpResponseMessage response = _client.GetAsync(Const.VersionUrl).Result;
            using HttpContent content = response.Content;
            return content.ReadAsStringAsync().Result;
#else
            using HttpResponseMessage response = _client.GetAsync(Const.ReleasesUrl).Result;
            using HttpContent content = response.Content;
            List<IncompleteRelease> releases = JsonSerializer.Deserialize<List<IncompleteRelease>>(content.ReadAsStringAsync().Result)!;
            return releases.Where(x => !x.upcoming_release).First().tag_name.Substring(1);
#endif
        }

        public static string GetVersionAddress(string versionString)
        {
            List<int> version = versionString.Split('.').ToList().ConvertAll(Convert.ToInt32);
#if DEBUG
            return Const.SlugChessUrl(version[0], version[1], version[2]);
#else
            using HttpResponseMessage response = _client.GetAsync($"{Const.GitlabProjectUrl}/releases/v{versionString}").Result;
            using HttpContent content = response.Content;
            var releaseString = content.ReadAsStringAsync().Result;
            //Console.WriteLine("Got contet " + releaseString);
            IncompleteRelease release = JsonSerializer.Deserialize<IncompleteRelease>(releaseString)!;
            //Console.WriteLine("Got contet " + release.assets.links.Count + "  " + release.tag_name + "  " + release.upcoming_release);

            return release.assets.links.Where(x => x.link_type == "package" && x.name == $"{Const.SlugchessFilename7z(version[0],version[1],version[2])}").First().url;
#endif
        }
    }
}
