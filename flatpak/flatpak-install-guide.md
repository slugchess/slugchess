

## generate nuget-sources
- ./flatpak-dotnet-generator.py slugchess-sources.json ../SlugChess/SlugChess.csproj 
- ./flatpak-dotnet-generator.py chesscom-sources.json ../ChesscomCSharpLib/ChesscomCSharpLib.csproj

### main-sorces.json contains lot of extra and manual sources that is needed.

## build package and put in reposotory-folder .repo
- flatpak-builder --repo=.repo --force-clean build-dir flatpak/com.slugchess.SlugChess.yml


## add the repo and install from it
- flatpak remote-add --no-gpg-verify test-repo .repo
- flatpak install test-repo com.slugchess.SlugChess